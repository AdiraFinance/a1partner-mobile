import 'package:adira_partner/constants/constanta.dart';
import 'package:flutter/material.dart';

class ChatPage extends StatefulWidget {
  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Chat SO D/M",
              style: TextStyle(fontFamily: "NunitoSans", color: Colors.black)),
          centerTitle: true,
          backgroundColor: kPrimaryColor,
        ),
        body: Container(
          child: Text("Chat Page"),
        ));
  }
}
