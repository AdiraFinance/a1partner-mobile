import 'package:adira_partner/constants/constanta.dart';
import 'package:adira_partner/constants/size_config.dart';
import 'package:adira_partner/db_helper/database_helper.dart';
import 'package:adira_partner/home.dart';
import 'package:adira_partner/provider/login_api_provider.dart';
import 'package:adira_partner/screens/login/dialog_terms_condition.dart';
import 'package:adira_partner/screens/login/upper_clipper.dart';
import 'package:flutter/material.dart';
import 'package:random_string/random_string.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  bool _secureText = true;
  Screen size;
  var _uid, _pwd, _ipAddress;
  var _autoValidate = false;
  final _key = new GlobalKey<FormState>();
  final _tecName = TextEditingController();
  final _tecUid = TextEditingController();
  final _tecPassword = TextEditingController();
  final _randomText = TextEditingController();
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _valueRrandomText = TextEditingController();
  LoginApiProvider _loginApiProvider;
  var _loginProcess = false;
  DbHelper _dbHelper = DbHelper();
  int _timeExpiredToken;
  bool _success;
  String _userEmail;

  _showHidePass() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  @override
  void initState() {
    super.initState();
    _loginApiProvider = LoginApiProvider();
    _setRandomText();
  }

  _setRandomText() {
    setState(() {
      _randomText.text = randomAlphaNumeric(4);
    });
  }

  @override
  Widget build(BuildContext context) {
    size = Screen(MediaQuery.of(context).size);
    return Scaffold(
      backgroundColor: Colors.black,
      key: _scaffoldKey,
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              _upperPart(),
              Container(
                margin: EdgeInsets.symmetric(horizontal: size.wp(8)),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      //flex: 4,
                      child: Container(),
                    ),
                    Expanded(
                        //child: Image.asset("img/ad1gate.webp",
                        //    width: 475, height: 75),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text("Adira1",
                                //textAlign: TextAlign.right,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 30,
                                    fontStyle: FontStyle.italic,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: "NunitoSansBold")),
                            Text("Partner",
                                //textAlign: TextAlign.right,
                                style: TextStyle(
                                    color: kPrimaryColor,
                                    fontSize: 30,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: "NunitoSansBold")),
                          ],
                        ),
                        flex: 7),
                  ],
                ),
              ),
              SizedBox(height: size.hp(1)),
              Form(
                key: _key,
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: size.wp(8)),
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        controller: _tecName,
                        style: new TextStyle(color: Colors.white),
                        decoration: new InputDecoration(
                          labelText: 'Name',
                          labelStyle: TextStyle(
                              fontFamily: "NunitoSans", color: Colors.white),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide(color: Colors.white)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide(color: Colors.white)),
                          errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide(color: Colors.red)),
                        ),
                        keyboardType: TextInputType.name,
                        onSaved: (e) => _uid = e,
                        validator: _validateEmail,
                        autovalidate: _autoValidate,
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      TextFormField(
                        controller: _tecUid,
                        style: new TextStyle(color: Colors.white),
                        decoration: new InputDecoration(
                          labelText: 'Email',
                          labelStyle: TextStyle(
                              fontFamily: "NunitoSans", color: Colors.white),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide(color: Colors.white)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide(color: Colors.white)),
                          errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide(color: Colors.red)),
                        ),
                        keyboardType: TextInputType.emailAddress,
                        onSaved: (e) => _uid = e,
                        validator: _validateEmail,
                        autovalidate: _autoValidate,
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      TextFormField(
                        controller: _tecPassword,
                        style: new TextStyle(color: Colors.white),
                        decoration: new InputDecoration(
                          labelText: 'Password',
                          labelStyle: TextStyle(
                              fontFamily: "NunitoSans", color: Colors.white),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide(color: Colors.white)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide(color: Colors.white)),
                          errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide(color: Colors.red)),
                          suffixIcon: IconButton(
                              icon: Icon(
                                _secureText
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                                color: Colors.white,
                              ),
                              onPressed: _showHidePass),
                        ),
                        keyboardType: TextInputType.emailAddress,
                        obscureText: _secureText,
                        onSaved: (e) => _pwd = e,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Can't be empty";
                          } else {
                            return null;
                          }
                        },
                        autovalidate: _autoValidate,
                      ),
                      SizedBox(height: 12),
                      Container(
                        alignment: Alignment.center,
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Text(
                          _success == null
                              ? ''
                              : (_success
                                  ? 'Successfully signed in ' + _userEmail
                                  : 'Sign in failed'),
                          style: TextStyle(color: Colors.red),
                        ),
                      ),
                      SizedBox(height: 24),
                      _loginProcess
                          ? Center(child: CircularProgressIndicator())
                          : RaisedButton(
                              color: Colors.cyan,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("Registration",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontFamily: "NunitoSansBold")),
                                ],
                              ),
                              padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
                              onPressed: () {
                                //  _testInsertLogin();
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => HomePage()));
                                //_check(context);
                              },
                            ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              new Align(
                // alignment: Alignment.bottomRight,
                alignment: const Alignment(1.0, 1.0),
                child: new Text('V.1.2.0    ',
                    style: new TextStyle(color: Colors.yellow, fontSize: 11)),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _upperPart() {
    return Stack(
      children: <Widget>[
        ClipPath(
          clipper: UpperClipper(),
          child: Container(
            height: size.hp(35), //getProportionateScreenHeight(8),
            decoration: BoxDecoration(color: Colors.cyan //myPrimaryColor
                ),
          ),
        ),
        Container(
            margin: EdgeInsets.only(top: size.hp(4), left: size.wp(8)),
            // top: getProportionateScreenHeight(4),
            // left: getProportionateScreenWidth(8)),
            child: Image.asset('assets/img/logo_adira.png',
                height: 150, width: 150))
      ],
    );
  }

  String _validateEmail(String value) {
    if (value.isEmpty) {
      // The form is empty
      return "Enter email address";
    }
    // This is just a regular expression for email addresses
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      // So, the email is valid
      return null;
    }
    // The pattern of the email didn't match the regex above.
    return 'Email is not valid';
  }

  _check(BuildContext context) {
    final form = _key.currentState;
    if (form.validate()) {
      form.save();
      _validasiRandomText();
    } else {
      setState(() {
        _autoValidate = true;
      });
    }
  }

  var _errorRandomNotmatch = false;
  _validasiRandomText() {
    if (_valueRrandomText.text != _randomText.text) {
      setState(() {
        _errorRandomNotmatch = true;
        _setRandomText();
      });
    } else {
      setState(() {
        _errorRandomNotmatch = false;
      });
      _goToHome(); //_login();
    }
  }

  _goToHome() {
    Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => HomePage(expiredToken: _timeExpiredToken)));
  }

  void _showSnackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: new Text(text,
          style: TextStyle(fontFamily: "NunitoSans", color: Colors.black)),
      behavior: SnackBarBehavior.floating,
      backgroundColor: Colors.white,
    ));
  }

  _savePref(String userDLC, String email, String DLCName) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    setState(() {
      _preferences.setString("userDLC", userDLC);
      _preferences.setString("email", email);
      _preferences.setString("DLCName", DLCName);
      _loginProcess = false;
    });
    _goToHome();
  }
}
