import 'dart:convert';
import 'dart:io';

import 'package:adira_partner/constants/url.dart';
import 'package:adira_partner/db_helper/database_helper.dart';
import 'package:http/http.dart' show Client;
import 'package:http/io_client.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GenerateDateApiProvider {
  DbHelper _dbHelper = DbHelper();
  Future<Map> generateDatesByAutoAssign(
      String zipCode, String kelurahan, String jenisKredit) async {
    List _resulTokenFromSQLIte = await _dbHelper.getToken();
    print("${_resulTokenFromSQLIte[0]['token']}");
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);

    try {
//      final _response = await _client.post("${BaseUrl.url}Submit/GenerateDatesByAutoAssign",
//          body: {
//            "ZipCode":zipCode,
//            "Kelurahan":kelurahan,
//            "JenisKredit":jenisKredit
//          }
//      );
      final _response = await http.post(
          "${BaseUrl.url}private/ad1gate/api/Submit/GenerateDatesByAutoAssign",
          body: {
            "ZipCode": zipCode,
            "Kelurahan": kelurahan,
            "JenisKredit": jenisKredit
          },
          headers: {
            'Authorization': 'Bearer ${_resulTokenFromSQLIte[0]['token']}'
          });
      print("cek status code ${_response.statusCode}");
      var _listDateByAutoAsign = [];
      final _data = jsonDecode(_response.body);
      if (_response.statusCode == 200) {
        for (var i = 0; i < _data['Data'].length; i++) {
          _listDateByAutoAsign.add(_data['Data'][i]);
        }
        var _result = {
          "status": true,
          "listDateByAutoAsign": _listDateByAutoAsign
        };
        return _result;
      } else {
        var _result = {"status": false, "data": _data};
        return _result;
      }
    } catch (e) {
      var _data = {"error_description": e.toString()};
      var _result = {"status": false, "data": _data};
      return _result;
    }
  }

  Future<Map> generateDatesByPemilihanCabang(
      String zipCode, String kelurahan, String jenisKredit) async {
//    Client _client = Client();

    List _resulTokenFromSQLIte = await _dbHelper.getToken();
    print("${_resulTokenFromSQLIte[0]['token']}");
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);

    try {
//      final _response = await _client.post(
//          "${BaseUrl.url}Submit/GenerateDatesByAutoBranch",
//          body: {
//            "ZipCode":zipCode,
//            "Kelurahan":kelurahan,
//            "JenisKredit":jenisKredit
//          },
//      );
      final _response = await http.post(
          "${BaseUrl.url}private/ad1gate/api/Submit/GenerateDatesByAutoBranch",
          body: {
            "ZipCode": zipCode,
            "Kelurahan": kelurahan,
            "JenisKredit": jenisKredit
          },
          headers: {
            'Authorization': 'Bearer ${_resulTokenFromSQLIte[0]['token']}'
          });

      final _data = jsonDecode(_response.body);

      if (_response.statusCode == 200) {
        var _listGenerateDateByBranch = [];
        for (var i = 0; i < _data["Data"].length; i++) {
          _listGenerateDateByBranch.add(_data["Data"][i]);
        }
        var _result = {
          "status": true,
          "listGenerateDateByBranch": _listGenerateDateByBranch
        };
        print("cek result $_result");
        return _result;
      } else {
        var _result = {"status": false, "data": _data};
        return _result;
      }
//      else{
//        final _data = jsonDecode(_response.body);
//        var _result = {"status":true,"message":_data["Message"]};
//        return _result;
//      }
    } catch (e) {
      var _data = {"error_description": e.toString()};
      var _result = {"status": false, "data": _data};
      return _result;
//      var _result = {"status":true,"message":e.toString()};
//      return _result;
    }
  }

  Future<Map> generateDatesByDadicateCMO(
      String zipCode, String kelurahan, String jenisKredit) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var DLCCode = _preferences.getString("userDLC");
    Client _client = Client();
    var _body = jsonEncode({
      "DLCCode": DLCCode,
      "ZipCode": zipCode,
      "Kelurahan": kelurahan,
      "JenisKredit": jenisKredit
    });
    try {
      print(_body);
      final _response = await _client.post(
          "${BaseUrl.url}private/ad1gate/api/Submit/GenerateDatesByDedicateCMO",
          headers: {"Content-Type": "application/json"},
          body: _body);
      print("cek mkt extend response status ${_response.statusCode}");
      var _listDateByDedicateCMO = [];
      if (_response.statusCode == 200) {
        final _data = jsonDecode(_response.body);
        print(_data);
        for (var i = 0; i < _data['Data'].length; i++) {
          _listDateByDedicateCMO.add(_data['Data'][i]);
          print(_listDateByDedicateCMO[i]);
        }
        var _result = {
          "status": true,
          "listDateByDedicateCMO": _listDateByDedicateCMO
        };
        return _result;
      } else {
        var _result = {"status": false, "message": "Failed get data"};
        return _result;
      }
    } catch (e) {
      var _result = {"status": false, "message": "Failed get data"};
      return _result;
    }
  }

  //add DAK
  Future<Map> generateDatesByNbm(
      String zipCode, String kelurahan, String jenisKredit) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var DLCCode = _preferences.getString("userDLC");

    List _resulTokenFromSQLIte = await _dbHelper.getToken();
    print("${_resulTokenFromSQLIte[0]['token']}");
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);
//    Client _client = Client();
//    var _body = jsonEncode({
//      "DLCCode":DLCCode,
//      "ZipCode":zipCode,
//      "Kelurahan":kelurahan,
//      "JenisKredit":jenisKredit
//    });
    try {
//      final _response = await _client.post(
//          "http://10.61.27.148/ad1gateapi/api/Submit/GenerateDatesByNBM",
//          headers: {"Content-Type":"application/json"},
//          body: _body
//      );
      final _response = await http.post(
          "${BaseUrl.url}private/ad1gate/api/Submit/GenerateDatesByNBM",
          headers: {
            'Authorization': 'Bearer ${_resulTokenFromSQLIte[0]['token']}'
          },
          body: {
            "DLCCode": DLCCode,
            "ZipCode": zipCode,
            "Kelurahan": kelurahan,
            "JenisKredit": jenisKredit
          });
      print("cek data NBM response status ${_response.statusCode}");
      var _listDateByNbm = [];
      final _data = jsonDecode(_response.body);

      if (_response.statusCode == 200) {
        print(_data);
        for (var i = 0; i < _data['Data'].length; i++) {
          _listDateByNbm.add(_data['Data'][i]);
          print(_listDateByNbm[i]);
        }
        var _result = {"status": true, "listDateByNbm": _listDateByNbm};
        return _result;
      } else {
        var _result = {"status": false, "data": _data};
        return _result;
      }
//      else{
//        var _result = {"status":false,"message":"Failed get data"};
//        return _result;
//      }
    } catch (e) {
      var _data = {"error_description": e.toString()};
      var _result = {"status": false, "data": _data};
      return _result;
//      var _result = {"status":false,"message":"Failed get data"};
//      return _result;
    }
  }

  Future<Map> generateDatesByAsIs(
      String zipCode, String kelurahan, String jenisKredit) async {
    Client _client = Client();
    try {
      final _response = await _client.post(
        "${BaseUrl.url}private/ad1gate/Submit/GenerateDatesByAsIs",
        body: {
          "ZipCode": zipCode,
          "Kelurahan": kelurahan,
          "JenisKredit": jenisKredit
        },
      );
      if (_response.statusCode == 200) {
        final _data = jsonDecode(_response.body);
        var _listGenerateDatesByAsIs = [];
        for (var i = 0; i < _data["Data"].length; i++) {
          _listGenerateDatesByAsIs.add(_data["Data"][i]);
        }
        var _result = {
          "status": true,
          "listGenerateDatesByAsIs": _listGenerateDatesByAsIs
        };
        print(_result);
        return _result;
      } else {
        final _data = jsonDecode(_response.body);
        var _result = {"status": true, "message": _data["Message"]};
        return _result;
      }
    } catch (e) {
      var _result = {"status": true, "message": e.toString()};
      return _result;
    }
  }
}
