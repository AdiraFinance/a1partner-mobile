import 'package:adira_partner/constants/constanta.dart';
import 'package:flutter/material.dart';

class DraftPage extends StatefulWidget {
  @override
  _DraftPageState createState() => _DraftPageState();
}

class _DraftPageState extends State<DraftPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Draft",
              style: TextStyle(fontFamily: "NunitoSans", color: Colors.black)),
          centerTitle: true,
          backgroundColor: kPrimaryColor,
        ),
        body: Container(
          child: Text("Draft Page"),
        ));
  }
}
