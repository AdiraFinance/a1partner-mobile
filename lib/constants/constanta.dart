import 'package:flutter/material.dart';

const kPrimaryColor =
    Colors.amberAccent; //Color(0xFFFFAB00); //Colors.cyan; //Color(0xFFFF7643);
const kPrimaryLightColor = Color(0xFFFFECDF);
const kSecondaryColor = Color(0xFF979797);
const kTextColor = Color(0xFF757575);

class Constant {
  static const String Logout = "Logout";

  static const List<String> choices = <String>[Logout];
}

//0xFFFFAB00
