import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:adira_partner/constants/url.dart';
import 'package:adira_partner/db_helper/database_helper.dart';
import 'package:http/http.dart' show Client;
import 'package:http/io_client.dart';

class SubmitOrderForm {
  DbHelper _dbHelper = DbHelper();

  Future<Map> submitOrder(var body) async {
    Client _client = Client();
    List _resulTokenFromSQLIte = await _dbHelper.getToken();
    print("${_resulTokenFromSQLIte[0]['token']}");
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);
//    var _body = jsonEncode(body);
    try {
//      final _response = await http.post(
//          "${BaseUrl.url}Submit/SubmitOrderAd1gate", headers: {"Content-Type":"application/json"},
//          body: _body
//      );
      final _response = await http.post(
          "${BaseUrl.url}private/ad1gate/api/Submit/SubmitOrderAd1gate",
          headers: {
            'Authorization': 'Bearer ${_resulTokenFromSQLIte[0]['token']}'
          },
          body: body);
      print("cek status${_response.statusCode}");
      final _data = jsonDecode(_response.body);
      print("cek response data $_data");
      if (_response.statusCode == 200) {
        var _result;
        if (_data['Status'] == 0) {
          _result = {
            "status": true,
            "message": _data['Message'],
            "data": _data['Data']
          };
        } else {
          var _dataError = {"error_description": _data['Message']};
          _result = {"status": false, "data": _dataError};
        }
        return _result;
      } else {
        var _result = {"status": false, "data": _data};
        return _result;
      }
    }
//    on TimeoutException catch(_){
//      var _result = {"status":false,"message":"Connection Timeout"};
//      return _result;
//    }
    catch (e) {
      var _dataError = {"error_description": e.toString()};
      var _result = {"status": false, "data": _dataError};
      return _result;
    }
  }
}
