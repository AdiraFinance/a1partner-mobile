import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/io_client.dart';

class LoginChangeNotifier with ChangeNotifier {
  String _username;
  String _password;

  bool _loadData = false;

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  String get username => _username;
  String get password => _password;
  bool get loadData => _loadData;

  set username(String value) {
    this._username = value;
    notifyListeners();
  }

  set password(String value) {
    this._password = value;
    notifyListeners();
  }

  set loadData(bool value) {
    this._loadData = value;
  }

  void getDataProfile() async {
    this._loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    try {
      var _body = jsonEncode(
          {"login": "ronny.suhermawan@adira.co.id", "password": "adira"});
      print(_body);

      final _response = await _http.post(
          "http://af-devnc04:99/CommonsWebAPI/api/PublicAccess/Authenticate",
          body: _body,
          headers: {"Content-Type": "application/json"});
      print(_response.statusCode);
      print(_response.body);

      // if (_response.statusCode == 200) {
      //   var _result = jsonDecode(_response.body);
      //   List _data = _result['data'];
      //   print(_result);
      //   if (_result.isEmpty) {
      //     print('a');
      //     showSnackBar("User tidak ditemukan");
      //   } else {
      //     print('b');
      //   }
      // } else {
      //   showSnackBar("Error response status ${_response.statusCode}");
      // }
    } catch (e) {
      showSnackBar("Error ${e.toString()}");
    }
    this._loadData = false;
    notifyListeners();
  }

  void showSnackBar(String text) {
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"),
        behavior: SnackBarBehavior.floating,
        backgroundColor: Colors.grey));
  }
}
