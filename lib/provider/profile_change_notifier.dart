import 'dart:convert';
import 'dart:io';

import 'package:adira_partner/model/profile_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/io_client.dart';
//import 'package:dio/dio.dart';

class ProfileChangeNotifier with ChangeNotifier {
  List<ProfileModel> _profile = [];
  String _username;
  //final _dio = new Dio();

  bool _loadData = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<ProfileModel> get profile => _profile;

  bool get loadData => _loadData;

  set loadData(bool value) {
    this._loadData = value;
  }

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  String get username => _username;

  set username(String value) {
    this._username = value;
    notifyListeners();
  }

  void getDataProfile() async {
    //SharedPreferences _preferences = await SharedPreferences.getInstance();
    //username = "diki.kusnendar@adira.co.id"; //_preferences.getString("username");

    this._profile.clear();
    this._loadData = true;
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;

    final _http = IOClient(ioc);

    try {
      var _body = jsonEncode({
        //"P_NIK": username,
        // "10065901"
      });
      //print(_body);

      print(username.toString());

      final _response = await _http.get(
          "http://10.81.3.137:1234/api/service/usermenu/findbyuserid/$username"
          // "https://103.110.89.34/public/ms2dev/api/tasklist/get-tasklist-ms2",
          // body: 'galaxi.023535@ad1gate.com',
          // headers: {"Content-Type": "application/json"}
          );
      // final response = await _dio
      //     .get("http://10.87.109.229:9090/api/ad1partner/get-user-detail");
      //print(_response.statusCode);
      //print(_response.body);

      if (_response.statusCode == 200) {
        print("masuk");
        print(_response.body);
        var _result = jsonDecode(_response.body);
        var _data = _result;
        print(_result);
        if (_result.isEmpty) {
          print('a');
          showSnackBar("Data Profile tidak ditemukan");
          // this._loadData = false;
        } else {
          print('b');
          this._profile.add(ProfileModel(
                _data['id'],
                _data['userId'],
                _data['userType'],
                _data['roleId'],
                _data['placeOfBirth'],
                //_birthDate,
                _data['birthDate'],
                _data['email'],
                _data['handphone'],
                _data['noWa'],
                _data['dealCode'],
                _data['dealName'],
                _data['createBy'],
                _data['createDate'],
                _data['modifyBy'],
                _data['modifyDate'],
              ));
          print('c');
          // this._loadData = false;
        }
      } else {
        showSnackBar("Error response status ${_response.statusCode}");
        // this._loadData = false;
      }
    } catch (e) {
      showSnackBar("Error ${e.toString()}");
      // this._loadData = false;
    }
    this._loadData = false;
    //print(this._loadData);
    notifyListeners();
  }

  void showSnackBar(String text) {
    this._scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: Text("$text"),
        behavior: SnackBarBehavior.floating,
        backgroundColor: Colors.grey));
  }
}
