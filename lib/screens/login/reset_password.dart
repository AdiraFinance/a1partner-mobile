import 'package:adira_partner/screens/login/change_password.dart';
import 'package:flutter/material.dart';

import 'package:adira_partner/constants/size_config.dart';

class ResetPasswordPage extends StatefulWidget {
  @override
  _ResetPasswordPageState createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> {
  Screen size;
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _key = new GlobalKey<FormState>();
  final _tecUid = TextEditingController();
  var _loginProcess = false;
  var _uid;
  var _autoValidate = false;

  @override
  Widget build(BuildContext context) {
    size = Screen(MediaQuery.of(context).size);
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.cyan, //myPrimaryColor,
        title: Text("Reset Password"),
        //automaticallyImplyLeading: false,
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Form(
                key: _key,
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: size.wp(8)),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 100),
                      Text(
                        "Silahkan input email yang akan di reset passwordnya dan sudah teregistrasi di system",
                        textAlign: TextAlign.start,
                      ),
                      SizedBox(height: 20),
                      TextFormField(
                        controller: _tecUid,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                          labelText: 'Email',
                          labelStyle: TextStyle(
                              fontFamily: "NunitoSans", color: Colors.black),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide(color: Colors.black)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide(color: Colors.black)),
                          errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide(color: Colors.red)),
                        ),
                        keyboardType: TextInputType.emailAddress,
                        onSaved: (e) => _uid = e,
                        validator: _validateEmail,
                        autovalidate: _autoValidate,
                      ),
                      SizedBox(height: 50),
                      _loginProcess
                          ? Center(child: CircularProgressIndicator())
                          : RaisedButton(
                              color: Colors.cyan, //myPrimaryColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("Submit",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontFamily: "NunitoSansBold")),
                                ],
                              ),
                              padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
                              onPressed: () {
                                //  _testInsertLogin();
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) =>
                                        ChangePasswordPage()));
                                //_check(context);
                              },
                            ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  String _validateEmail(String value) {
    if (value.isEmpty) {
      // The form is empty
      return "Enter email address";
    }
    // This is just a regular expression for email addresses
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      // So, the email is valid
      return null;
    }
    // The pattern of the email didn't match the regex above.
    return 'Email is not valid';
  }
}
