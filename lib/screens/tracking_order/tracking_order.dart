import 'package:adira_partner/constants/constanta.dart';
import 'package:flutter/material.dart';

class TrackingOrderPage extends StatefulWidget {
  @override
  _TrackingOrderPageState createState() => _TrackingOrderPageState();
}

class _TrackingOrderPageState extends State<TrackingOrderPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Tracking Order",
              style: TextStyle(fontFamily: "NunitoSans", color: Colors.black)),
          centerTitle: true,
          backgroundColor: kPrimaryColor,
        ),
        body: Container(
          child: Text("Tracking Order Page"),
        ));
  }
}
