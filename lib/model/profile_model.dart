class ProfileModel {
  final int id;
  final String userId;
  final String userType;
  final String roleId;
  final String placeOfBirth;
  final String birthDate;
  final String email;
  final String handphone;
  final String noWa;
  final String dealCode;
  final String dealName;
  final String createBy;
  final String createDate;
  final String modifyBy;
  final String modifyDate;

  ProfileModel(
      this.id,
      this.userId,
      this.userType,
      this.roleId,
      this.placeOfBirth,
      this.birthDate,
      this.email,
      this.handphone,
      this.noWa,
      this.dealCode,
      this.dealName,
      this.createBy,
      this.createDate,
      this.modifyBy,
      this.modifyDate);
}
