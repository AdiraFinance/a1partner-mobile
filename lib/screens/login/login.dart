import 'package:adira_partner/constants/constanta.dart';
import 'package:adira_partner/constants/size_config.dart';
import 'package:adira_partner/db_helper/database_helper.dart';
import 'package:adira_partner/screens/login/dialog_terms_condition.dart';
import 'package:adira_partner/home.dart';
import 'package:adira_partner/provider/login_api_provider.dart';
import 'package:adira_partner/screens/login/reset_password.dart';
import 'package:adira_partner/screens/login/signup.dart';
import 'package:adira_partner/screens/login/upper_clipper.dart';
//import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get_ip/get_ip.dart';
import 'package:random_string/random_string.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _secureText = true;
  Screen size;
  var _uid, _pwd, _ipAddress;
  var _autoValidate = false;
  final _key = new GlobalKey<FormState>();
  final _tecUid = TextEditingController()..text = 'galaxi.023535@ad1gate.com';
  final _tecPassword = TextEditingController()..text = 'adira';
  final _randomText = TextEditingController();
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _valueRrandomText = TextEditingController();
  LoginApiProvider _loginApiProvider;

  //new
  double getWidhtScreen(BuildContext context) =>
      MediaQuery.of(context).size.width;

  var _loginProcess = false;
  DbHelper _dbHelper = DbHelper();
  int _timeExpiredToken;
  bool _success;
  String _userEmail;

  String _userCoba = 'galaxi.023535@ad1gate.com';
  String _passCoba = 'adira';
  //bool _success = false;
  //final FirebaseAuth _auth = FirebaseAuth.instance;

  _showHidePass() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  @override
  void initState() {
    super.initState();
    _loginApiProvider = LoginApiProvider();
    _getIpAddress();
    _setRandomText();
    _checkAvailToken();
  }

  _setRandomText() {
    setState(() {
      _randomText.text = randomAlphaNumeric(4);
    });
  }

  _getIpAddress() async {
    String ipAddress = await GetIp.ipAddress;
    setState(() {
      _ipAddress = ipAddress;
    });
  }

  _checkLoginDate() async {
    setState(() {
      _loginProcess = true;
    });
    var _result = await _dbHelper.getDateLogin();
    if (_result.length > 0) {
      setState(() {
        _loginProcess = false;
      });
      _goToHome();
    } else {
      setState(() {
        _loginProcess = false;
      });
      return;
    }
  }

  _checkAvailToken() async {
    setState(() {
      _loginProcess = true;
    });
    var _result = await _dbHelper.getToken();

    if (_result.length > 0) {
      // if (_result[0]['token'] != "") {
      //   var _resultCheckToken = await _loginApiProvider.checkValidToken();
      //   if (_resultCheckToken['result']) {
      //     setState(() {
      //       _timeExpiredToken = _resultCheckToken['data']['exp'];
      //       _loginProcess = false;
      //     });
      //     _savePref(
      //         _resultCheckToken['data']['UserDetail'][0]['UserDLC'],
      //         _resultCheckToken['data']['UserDetail'][0]['ObjectAlias'],
      //         _resultCheckToken['data']['DLCName']);
      //   } else {
      //     setState(() {
      //       _loginProcess = false;
      //     });
      //     _showSnackBar(_resultCheckToken['data']['error_description']);
      //   }
      // } else {
      setState(() {
        _loginProcess = false;
      });
      // }
    } else {
      setState(() {
        _loginProcess = false;
      });
      return;
    }
  }

  @override
  Widget build(BuildContext context) {
    //new
    double smallDiameter = getWidhtScreen(context) * 0.6;
    double bigDiameter = getWidhtScreen(context) * 0.8;

    size = Screen(MediaQuery.of(context).size);
    return Scaffold(
      backgroundColor: Color(0xFFEEEEEE), //Colors.black,
      key: _scaffoldKey,
      body: Stack(
        children: [
          // Layer 1: Small Circle on Top Right
          Positioned(
            right: -smallDiameter * 0.2,
            top: -smallDiameter * 0.5,
            child: Container(
              width: smallDiameter,
              height: smallDiameter,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [Colors.amber[300], Colors.amber[100]]),
              ),
            ),
          ),

          // Layer 2: Big Circle on Top Left
          Positioned(
            left: -bigDiameter * 0.2,
            top: -bigDiameter * 0.25,
            child: Container(
              width: bigDiameter,
              height: bigDiameter,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.amber[500], Colors.amber[100]],
                ),
              ),
            ),
          ),

          // Layer 3: Text :: Welcome to Ad1Parter
          Positioned(
            left: 20,
            top: 80,
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Welcome to",
                    style: TextStyle(fontSize: 20, color: Colors.grey[700]),
                  ),
                  Text(
                    "Ad1Partner",
                    style: TextStyle(fontSize: 30, color: Colors.grey[700]),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(30, 3, 0, 0),
                    child: Text(
                      "Supported by Adira Finance",
                      style: TextStyle(fontSize: 12, color: Colors.grey[700]),
                    ),
                  ),
                ],
              ),
            ),
          ),

          // Layer 4: Small Circle on Bottom Left
          Positioned(
            right: -smallDiameter * 0.3,
            bottom: -smallDiameter * 0.5,
            child: Container(
              width: smallDiameter,
              height: smallDiameter,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [Colors.grey[50], Colors.amber[200]])),
            ),
          ),

          // Layer 5: Text ::  Copyright
          Positioned(
            right: 25,
            bottom: 20,
            child: Container(
              child: Text(
                "Copyright @ 2021",
                style: TextStyle(color: Colors.grey[700], fontSize: 11),
              ),
            ),
          ),

          Align(
            alignment: Alignment.bottomCenter,
            child: ListView(
              children: [
                // Container: Login Card
                buildContainerLoginCard(context),
                // Container: Social Media
                buildContainerSocialMedia(),
                // Container: Don't have an account
                buildContainerSignUp()
              ],
            ),
          )
        ],
      ),
    );
  }

// Container: Login Card
  Container buildContainerLoginCard(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(10, 250, 10, 0),
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(4)),
      child: Card(
        elevation: 8,
        child: Container(
          padding: EdgeInsets.fromLTRB(15, 20, 20, 15),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4),
            color: kPrimaryColor, //Colors.amberAccent,
          ),
          child: Column(
            children: [
              // Column 1: Textfile Username
              TextField(
                controller: _tecUid,
                decoration: InputDecoration(
                  icon: Icon(
                    Icons.person,
                    color: Colors.grey[700],
                  ),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey[700])),
                  labelText: "Username",
                  labelStyle: TextStyle(color: Colors.grey),
                ),
              ),

              // Column 2: Textfield Password
              TextField(
                controller: _tecPassword,
                obscureText: true,
                decoration: InputDecoration(
                    icon: Icon(
                      Icons.vpn_key,
                      color: Colors.grey[700],
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey[700]),
                    ),
                    labelText: "Password",
                    labelStyle: TextStyle(color: Colors.grey)),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(35, 20, 0, 0),
                child: Row(
                  children: [
                    // Row 1: Login Button
                    _loginProcess
                        ? Center(
                            child: CircularProgressIndicator(
                            backgroundColor: Colors.black,
                          ))
                        : RaisedButton(
                            elevation: 5,
                            child: Text(
                              "Sign In",
                              style: TextStyle(color: Colors.amber[100]),
                            ),
                            color: Colors.grey[600],
                            shape: StadiumBorder(),
                            onPressed: () {
                              Future.delayed(const Duration(seconds: 1),
                                  () => _loginTest());
                            },
                          ),
                    Spacer(flex: 3),
                    // Row 2: Forgot Password
                    Text(
                      "Forgot Password?",
                      style: TextStyle(color: Colors.grey[700]),
                      textAlign: TextAlign.right,
                    ),
                    Spacer(flex: 1)
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  // Container: Social Media
  Container buildContainerSocialMedia() {
    return Container(
      margin: EdgeInsets.fromLTRB(25, 5, 25, 0),
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          SizedBox(
            width: 30,
            height: 30,
            child: FloatingActionButton(
                elevation: 2,
                mini: true,
                heroTag: null,
                backgroundColor: kPrimaryColor, //Colors.amberAccent,
                child: Image(
                  image: AssetImage("assets/img/instagram.png"),
                  color: Colors.grey[700],
                ),
                onPressed: () {}),
          ),
          Spacer(),
          SizedBox(
            height: 30,
            child: Center(
              child: Text(
                "@adirafinanceid",
                style: TextStyle(color: Colors.grey[700]),
              ),
            ),
          ),
          Spacer(flex: 6),
          SizedBox(
            width: 30,
            height: 30,
            child: FloatingActionButton(
                elevation: 2,
                mini: true,
                heroTag: null,
                backgroundColor: Colors.amberAccent,
                child: Image(
                  image: AssetImage("assets/img/twitter.png"),
                  color: Colors.grey[700],
                ),
                onPressed: () {}),
          ),
          Spacer(),
          SizedBox(
            height: 30,
            child: Center(
              child: Text(
                "@adirafinanceid",
                style: TextStyle(color: Colors.grey[700]),
              ),
            ),
          ),
        ],
      ),
    );
  }

  // Container: Don't have an account
  Container buildContainerSignUp() {
    return Container(
      margin: EdgeInsets.fromLTRB(25, 20, 25, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Don't have an account?  ",
            style: TextStyle(color: Colors.grey[700]),
          ),
          Text(
            "SIGN UP",
            style: TextStyle(color: Colors.amber[600]),
          ),
          // Navigator.of(context)
          //             .push(MaterialPageRoute(builder: (context) => SignUp()));
        ],
      ),
    );
  }

  String _validateEmail(String value) {
    if (value.isEmpty) {
      // The form is empty
      return "Enter email address";
    }
    // This is just a regular expression for email addresses
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      // So, the email is valid
      return null;
    }
    // The pattern of the email didn't match the regex above.
    return 'Email is not valid';
  }

  _check(BuildContext context) {
    final form = _key.currentState;
    if (form.validate()) {
      form.save();
      _loginTest();
      //_validasiRandomText();
    } else {
      setState(() {
        _autoValidate = true;
      });
    }
  }

  // var _errorRandomNotmatch = false;
  // _validasiRandomText() {
  //   if (_valueRrandomText.text != _randomText.text) {
  //     setState(() {
  //       _errorRandomNotmatch = true;
  //       _setRandomText();
  //     });
  //   } else {
  //     setState(() {
  //       _errorRandomNotmatch = false;
  //     });
  //     //_login();
  //     _loginTest();
  //   }
  // }

  _loginTest() async {
    setState(() {
      _loginProcess = true;
    });

    print(_tecUid.text);
    print(_tecPassword.text);
    var _result =
        await _loginApiProvider.loginTest(_tecUid.text, _tecPassword.text);
    print(_result);
    //if ((_tecUid.text == _userCoba) && (_tecPassword.text == _passCoba)) {
    if (_result['Message'] == 'OK') {
      setState(() {
        _success = true;
        _loginProcess = false;
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => HomePage()));
      });
    } else {
      setState(() {
        _success = false;
        _loginProcess = false;
        _showSnackBar("Login Failed. User and Password not valid.");
        //_tecUid.dispose();
        //_tecPassword.dispose();
      });
    }
  }

  _goToHome() {
    Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => HomePage(expiredToken: _timeExpiredToken)));
  }

  void _showSnackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: new Text(text,
          style: TextStyle(fontFamily: "NunitoSans", color: Colors.black)),
      behavior: SnackBarBehavior.floating,
      backgroundColor: Colors.white,
    ));
  }

  // _savePref(String userDLC, String email, String DLCName) async {
  //   SharedPreferences _preferences = await SharedPreferences.getInstance();
  //   setState(() {
  //     _preferences.setString("userDLC", userDLC);
  //     _preferences.setString("email", email);
  //     _preferences.setString("DLCName", DLCName);
  //     _loginProcess = false;
  //   });
  //   _goToHome();
  // }

  // void submit() async {
  //   final form = _key.currentState;
  //   form.save();
  //   print(_tecUid.text + _tecPassword.text);
  //   //final FirebaseUser user = await _auth.currentUser();
  //   try {
  //     // final auth = Provider.of(context).auth;
  //     // String uid = await auth.signInWithEmailAndPassword(
  //     //     _tecUid.text, _tecPassword.text);
  //     // final FirebaseUser user = (await _auth.signInWithEmailAndPassword(
  //     //   email: _tecUid.text,
  //     //   password: _tecPassword.text,
  //     // ));
  //     // print("Signed In with ID $user");
  //     //Navigator.of(context).pushReplacementNamed('/home');
  //     Navigator.of(context)
  //         .push(MaterialPageRoute(builder: (context) => HomePage()));
  //   } catch (e) {
  //     print(e);
  //   }
  // }
}
