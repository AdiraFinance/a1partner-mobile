import 'dart:convert';
import 'dart:io';

import 'package:adira_partner/constants/url.dart';
import 'package:adira_partner/db_helper/database_helper.dart';
import 'package:http/http.dart' show Client;
import 'package:http/io_client.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GetDataModelKendaraanApiProvider {
  Client _client = Client();
  DbHelper _dbHelper = DbHelper();
  Future<Map> getModelKendaraan(String model, String idStatusKendaraan) async {
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var dlc = _preferences.getString("userDLC");

    List _resulTokenFromSQLIte = await _dbHelper.getToken();
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);

    try {
//      final _response = await _client.post(
//        "${BaseUrl.url}Submit/GetModel",
//        body: {
//          "search": model,
//          "branchid": "0101",
//          "groupcode": idStatusKendaraan,
//          "dlc": dlc
//        }
//      );
      print("testmodel");
      final _response = await http
          .post("${BaseUrl.url}private/ad1gate/api/Submit/GetModel", body: {
        "search": model,
        "branchid": "0101",
        "groupcode": idStatusKendaraan,
        "dlc": "049925" //dlc
      });
      final _data = jsonDecode(_response.body);
      print("cek data model kendaraan $_data");
      if (_response.statusCode == 200) {
        var _listModelKendaraan = [];
        for (var i = 0; i < _data['Data'].length; i++) {
          _listModelKendaraan.add(_data['Data'][i]);
        }
        var _result = {
          "status": true,
          "listModelKendaraan": _listModelKendaraan
        };
        print(_result);
        return _result;
      } else {
        var _result = {"status": false, "data": _data};
        return _result;
      }
    } catch (e) {
      var _dataError = {"error_description": "${e.toString()}"};
      var _result = {"status": false, "data": _dataError};
      return _result;
    }
  }
}
