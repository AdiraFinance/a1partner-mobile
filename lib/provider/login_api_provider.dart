import 'dart:convert';
import 'dart:io';
import 'package:adira_partner/db_helper/database_helper.dart';
import 'package:adira_partner/constants/url.dart';
import 'package:http/http.dart' show Client;
import 'package:http/io_client.dart';

class LoginApiProvider {
  Client client = Client();
  DbHelper _dbHelper = DbHelper();

  Future<Map> loginProcess(String uid, String pwd, String ipAddress) async {
    print("uid $uid");
    print("pwd $pwd");
    print("ipAddress $ipAddress");
    try {
      final _response = await client.post("${BaseUrl.url}login/authenticate",
          body: {"uid": uid, "pwd": pwd, "ipAddress": ipAddress});
      final _data = jsonDecode(_response.body);
      if (_data['Data'] != null) {
        print("cek data dari server ${_data['Data']}");
        print(_data['Message']);
        var _result = {
          "message": _data['Message'],
          "userDlc": _data['Data']['UserDetail'][0]['UserDLC'],
          "DLCName": _data['Data']['DLCName']
        };
        print(_data['Data']['UserDetail'][0]['UserDLC']);
        print(_data['Data']['DLCName']);
        return _result;
      } else {
        var _result = {"message": _data['Message']};
        return _result;
      }
    } catch (e) {
      print(e.toString());
      var _result = {"status": false, "message": e.toString()};
      return _result;
    }
  }

  Future<Map> loginTest(String uid, String pwd) async {
    //print("uid $uid");
    //print("pwd $pwd");

    try {
      final _response = await client.post(
          "http://10.81.3.61:99/CommonsWebAPI/api/PublicAccess/Authenticate",
          body: {"login": uid, "password": pwd});
      final _data = jsonDecode(_response.body);
      return _data;
    } catch (e) {
      print(e.toString());
      var _result = {"status": false, "message": e.toString()};
      return _result;
    }
  }

  // Future<Map> loginToken(String email, String password) async {
  //   String _username = 'ad1gate_mobile_uat';
  //   String _password = 'secret';

  //   String basicAuth =
  //       'Basic ' + base64Encode(utf8.encode('$_username:$_password'));
  //   print(basicAuth);

  //   final ioc = new HttpClient();
  //   ioc.badCertificateCallback =
  //       (X509Certificate cert, String host, int port) => true;
  //   final http = new IOClient(ioc);

  //   try {
  //     final _response =
  //         await http.post("${BaseUrl.url}public/security/oauth/token", body: {
  //       "username": email,
  //       "password": password,
  //       "grant_type": "password",
  //       "client_id": "ad1gate_mobile_uat"
  //     }, headers: {
  //       'authorization': basicAuth
  //     });
  //     final _responseData = jsonDecode(_response.body);
  //     print(_responseData);

  //     var _result;

  //     if (_response.statusCode == 200) {
  //       _result = {"result": true, "data": _responseData};
  //     } else {
  //       _result = {"result": false, "message": "Failed get data"};
  //     }
  //     return _result;
  //   } catch (e) {
  //     print("cek error ${e.toString()}");
  //     var _result = {"result": false, "message": e.toString()};
  //     return _result;
  //   }
  // }

  // Future<Map> checkValidToken() async {
  //   List _result = await _dbHelper.getToken();

  //   String _username = 'ad1gate_mobile_uat';
  //   String _password = 'secret';
  //   String basicAuth =
  //       'Basic ' + base64Encode(utf8.encode('$_username:$_password'));
  //   print(basicAuth);

  //   final ioc = new HttpClient();
  //   ioc.badCertificateCallback =
  //       (X509Certificate cert, String host, int port) => true;
  //   final http = new IOClient(ioc);

  //   try {
  //     final _response = await http
  //         .post("${BaseUrl.url}public/security/oauth/check_token", body: {
  //       "token": _result[0]['token'],
  //     }, headers: {
  //       'authorization': basicAuth
  //     });
  //     final _responseData = jsonDecode(_response.body);
  //     var _resultData;
  //     if (_response.statusCode == 200) {
  //       _resultData = {"result": true, "data": _responseData};
  //     } else {
  //       _resultData = {"result": false, "data": _responseData};
  //     }
  //     return _resultData;
  //   } catch (e) {
  //     print(e.toString());
  //     var _data = {"error_description": e.toString()};
  //     var _resultError = {"result": false, "data": _data};
  //     return _resultError;
  //   }
  // }

  Future<String> logout() async {
    List _result = await _dbHelper.getToken();
    print(_result[0]['token']);

    String _username = 'ad1gate_mobile_uat';
    String _password = 'secret';

    String basicAuth =
        'Basic ' + base64Encode(utf8.encode('$_username:$_password'));
    print(basicAuth);

    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);

    try {
      final _response = await http.post(
          "${BaseUrl.url}public/security/oauth/revoke-token",
          headers: {'Authorization': basicAuth, 'token': _result[0]['token']});
      print(_response.statusCode);
      return _response.statusCode.toString();
    } catch (e) {
      throw ("Logout failed");
    }
  }
}
