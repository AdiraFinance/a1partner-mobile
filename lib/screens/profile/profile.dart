import 'package:adira_partner/constants/constanta.dart';
import 'package:adira_partner/provider/profile_change_notifier.dart';
import 'package:adira_partner/screens/login/change_password.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
//import 'package:adira_partner/main.dart';

class ProfilePage extends StatefulWidget {
  final String email, name;

  const ProfilePage({Key key, this.email, this.name}) : super(key: key);
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  //ProfileApiProvider _profileApiProvider;
  List _dataProfile = [];

  @override
  void initState() {
    super.initState();
    //_profileApiProvider = ProfileApiProvider();
    _getProfile();
  }

  _getProfile() async {
    Provider.of<ProfileChangeNotifier>(context, listen: false).getDataProfile();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: Provider.of<ProfileChangeNotifier>(context, listen: false)
          .scaffoldKey,
      appBar: AppBar(
        title: Text("Profile Detail",
            style: TextStyle(fontFamily: "NunitoSans", color: Colors.black)),
        centerTitle: true,
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[kPrimaryColor, Colors.grey[350]])),
        ),
        //backgroundColor: kPrimaryColor,
      ),
      body: Container(child:
          Consumer<ProfileChangeNotifier>(builder: (context, value, child) {
        return value.loadData
            ? Center(child: CircularProgressIndicator())
            : value.profile.isNotEmpty
                ? ListView(
                    padding: EdgeInsets.all(12),
                    children: <Widget>[
                      Text('Profile',
                          style: TextStyle(
                              fontFamily: 'NunitoSansSemiBold', fontSize: 18)),
                      SizedBox(height: 12),
                      Row(children: <Widget>[
                        Expanded(
                            flex: 2,
                            child: Text("User ID",
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                        Expanded(
                            flex: 0,
                            child: Text(" : ",
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                        Expanded(
                            flex: 5,
                            child: Text("${value.profile[0].userId}",
                                //value.taskList[index].LAST_KNOWN_STATE,
                                //value.profile[0].userID,
                                //"diki.kusnendar@adira.co.id",
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                      ]),
                      SizedBox(height: 32),
                      Text('Information User',
                          style: TextStyle(
                              fontFamily: 'NunitoSansSemiBold', fontSize: 18)),
                      SizedBox(height: 12),
                      Row(children: <Widget>[
                        Expanded(
                            flex: 2,
                            child: Text("Name",
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                        Expanded(
                            flex: 0,
                            child: Text(" : ",
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                        Expanded(
                            flex: 5,
                            child: Text("${value.profile[0].dealName}",
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                      ]),
                      SizedBox(height: 12),
                      Row(children: <Widget>[
                        Expanded(
                            flex: 2,
                            child: Text("PoB",
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                        Expanded(
                            flex: 0,
                            child: Text(" : ",
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                        Expanded(
                            flex: 5,
                            child: Text("${value.profile[0].placeOfBirth}",
                                //"Banjar",
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                      ]),
                      SizedBox(height: 12),
                      Row(children: <Widget>[
                        Expanded(
                            flex: 2,
                            child: Text("DoB",
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                        Expanded(
                            flex: 0,
                            child: Text(" : ",
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                        Expanded(
                            flex: 5,
                            child: Text("${value.profile[0].birthDate}",
                                //"12 Desember 1988",
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                      ]),
                      SizedBox(height: 12),
                      Row(children: <Widget>[
                        Expanded(
                            flex: 2,
                            child: Text("Email",
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                        Expanded(
                            flex: 0,
                            child: Text(" : ",
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                        Expanded(
                            flex: 5,
                            child: Text("${value.profile[0].email}",
                                //"diki.kusnendar@gmail.com",
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                      ]),
                      SizedBox(height: 12),
                      Row(children: <Widget>[
                        Expanded(
                            flex: 2,
                            child: Text("No HP",
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                        Expanded(
                            flex: 0,
                            child: Text(" : ",
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                        Expanded(
                            flex: 5,
                            child: Text("${value.profile[0].handphone}",
                                //"081210931470",
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                      ]),
                      SizedBox(height: 12),
                      Row(children: <Widget>[
                        Expanded(
                            flex: 2,
                            child: Text("No WA",
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                        Expanded(
                            flex: 0,
                            child: Text(" : ",
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                        Expanded(
                            flex: 5,
                            child: Text("${value.profile[0].noWa}",
                                //"081210931470",
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                      ]),
                      SizedBox(height: 12),
                      Row(children: <Widget>[
                        Expanded(
                            flex: 2,
                            child: Text("Dealer",
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                        Expanded(
                            flex: 0,
                            child: Text(" : ",
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                        Expanded(
                            flex: 5,
                            child: Text(
                                value.profile[0].dealCode != null
                                    ? "${value.profile[0].dealCode} - ${value.profile[0].dealName}"
                                    : '',
                                style: TextStyle(
                                    fontFamily: "NunitoSans", fontSize: 16))),
                      ]),
                      SizedBox(height: 24),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            flex: 1,
                            child: RaisedButton(
                              color: kPrimaryColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("Edit Profile",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontFamily: "NunitoSansBold")),
                                ],
                              ),
                              padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) =>
                                        ChangePasswordPage()));
                              },
                            ),
                          ),
                          SizedBox(width: 12),
                          Expanded(
                            flex: 1,
                            child: RaisedButton(
                              color: kPrimaryColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("Change Password",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontFamily: "NunitoSansBold")),
                                ],
                              ),
                              padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) =>
                                        ChangePasswordPage()));
                              },
                            ),
                          ),
                        ],
                      ),
                    ],
                  )
                : Center(
                    child: Text(
                    "Data Profile Tidak Ditemukan",
                    style: TextStyle(color: Colors.grey, fontSize: 16),
                  ));
      })),
    );
  }

  // void _getProfile() async {
  //   var _result =
  //       await _profileApiProvider.getProfile("diki.kusnendar@adira.co.id");
  //   List tList = [];
  //   print("cek length :${_result['data'].length}");
  //   for (var i = 0; i < _result['data'].length; i++) {
  //     tList.add(_result['data'][i]);
  //   }

  //   setState(() {
  //     _dataProfile.addAll(tList);
  //   });

  //   print(_dataProfile[0]['userID']);
  // }
}
