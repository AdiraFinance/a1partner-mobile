import 'package:adira_partner/constants/constanta.dart';
import 'package:flutter/material.dart';

import 'package:adira_partner/home.dart';
import 'package:adira_partner/constants/size_config.dart';

class ChangePasswordPage extends StatefulWidget {
  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  Screen size;
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _key = new GlobalKey<FormState>();
  final _tecPassword = TextEditingController();
  final _tecRePassword = TextEditingController();
  var _loginProcess = false;
  var _autoValidate = false;
  bool _secureText = true;
  var _pwd, _repwd;

  _showHidePass() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    size = Screen(MediaQuery.of(context).size);
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        backgroundColor: kPrimaryColor, //Colors.cyan, //myPrimaryColor,
        title: Text("Change Password"),
        //automaticallyImplyLeading: false,
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Form(
                key: _key,
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: size.wp(8)),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 100),
                      Text(
                        "Please enter a new password",
                        textAlign: TextAlign.start,
                      ),
                      SizedBox(height: 20),
                      TextFormField(
                        controller: _tecPassword,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                          labelText: 'New Password',
                          labelStyle: TextStyle(
                              fontFamily: "NunitoSans", color: Colors.black),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide(color: Colors.black)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide(color: Colors.black)),
                          errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide(color: Colors.red)),
                          suffixIcon: IconButton(
                              icon: Icon(
                                _secureText
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                                color: Colors.black,
                              ),
                              onPressed: _showHidePass),
                        ),
                        keyboardType: TextInputType.emailAddress,
                        obscureText: _secureText,
                        onSaved: (e) => _pwd = e,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Can't be empty";
                          } else {
                            return null;
                          }
                        },
                        autovalidate: _autoValidate,
                      ),
                      SizedBox(height: 20),
                      TextFormField(
                        controller: _tecRePassword,
                        style: new TextStyle(color: Colors.black),
                        decoration: new InputDecoration(
                          labelText: 'Re-enter Password',
                          labelStyle: TextStyle(
                              fontFamily: "NunitoSans", color: Colors.black),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide(color: Colors.black)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide(color: Colors.black)),
                          errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0),
                              borderSide: BorderSide(color: Colors.red)),
                          suffixIcon: IconButton(
                              icon: Icon(
                                _secureText
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                                color: Colors.black,
                              ),
                              onPressed: _showHidePass),
                        ),
                        keyboardType: TextInputType.emailAddress,
                        obscureText: _secureText,
                        onSaved: (e) => _repwd = e,
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Can't be empty";
                          } else {
                            return null;
                          }
                        },
                        autovalidate: _autoValidate,
                      ),
                      SizedBox(height: 50),
                      _loginProcess
                          ? Center(child: CircularProgressIndicator())
                          : RaisedButton(
                              color:
                                  kPrimaryColor, //Colors.cyan, //myPrimaryColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("Save Password",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontFamily: "NunitoSansBold")),
                                ],
                              ),
                              padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
                              onPressed: () {
                                //  _testInsertLogin();
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => HomePage()));
                                //_check(context);
                              },
                            ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
