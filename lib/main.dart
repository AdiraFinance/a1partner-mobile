import 'dart:async';
import 'dart:io';

import 'package:adira_partner/db_helper/database_helper.dart';
import 'package:adira_partner/provider/profile_change_notifier.dart';
import 'package:adira_partner/screens/login/login.dart';
import 'package:file_utils/file_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:location/location.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart' as handler;
import 'package:provider/provider.dart';

import 'constants/constanta.dart';
import 'package:adira_partner/provider/search_delegate_dealer_cabang.dart';

void main() {
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider(create: (context) => ProfileChangeNotifier()),
  ], child: MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Adira Finance',
      localizationsDelegates: [CustomLocalizationDelegate()],
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.yellow,
//        accentColor: Colors.black,
      ),
      home: MyHomePage(title: 'Adira Finance'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Location _locationService = new Location();
  bool _permission = false;
  String error;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end:
              Alignment(0.2, 0.0), // 10% of the width, so there are ten blinds.
          colors: [
            const Color(0xffffc107),
            const Color(0xffffecb3)
          ], // red to yellow
          tileMode: TileMode.clamp, // repeats the gradient over the canvas
        ),
        // backgroundColor: Colors.blue, //kPrimaryColor,
        // body: Center(
        //   child: Image.asset('assets/img/logo_adira.png'),
      ),
      child: Center(child: Image.asset('assets/img/logo_adira.png')),
    );
  }

  DbHelper _dbHelper = DbHelper();

  @override
  void initState() {
    super.initState();
    checkPermissionLocation();
    initDatabase();
  }

  initDatabase() async {
    _dbHelper.initDatabase();
  }

  Directory externalDir;
  var path = "No Data";
  String dirLoc = "";
  handler.PermissionStatus permissionStatusStorage;
  handler.PermissionStatus permissionStatusLocation;

  checkPermissionLocation() async {
    await _locationService.changeSettings(
        accuracy: LocationAccuracy.HIGH, interval: 1000);

    bool serviceStatus = await _locationService.serviceEnabled();

    if (serviceStatus) {
      permissionStatusLocation = await handler.PermissionHandler()
          .checkPermissionStatus(handler.PermissionGroup.location);
      if (permissionStatusLocation == handler.PermissionStatus.denied) {
        Map<handler.PermissionGroup, handler.PermissionStatus> permissions =
            await handler.PermissionHandler()
                .requestPermissions([handler.PermissionGroup.location]);
        checkPermissionStorage();
      } else {
        checkPermissionStorage();
      }
    } else {
      bool serviceStatusResult = await _locationService.requestService();
      if (serviceStatusResult) {
        permissionStatusLocation = await handler.PermissionHandler()
            .checkPermissionStatus(handler.PermissionGroup.location);
        if (permissionStatusLocation == handler.PermissionStatus.denied) {
          var permissions = await handler.PermissionHandler()
              .requestPermissions([handler.PermissionGroup.location]);
          checkPermissionStorage();
        } else {
          checkPermissionStorage();
        }
      } else {
        SystemChannels.platform.invokeMethod('SystemNavigator.pop');
      }
    }
  }

  checkPermissionStorage() async {
    permissionStatusStorage = await handler.PermissionHandler()
        .checkPermissionStatus(handler.PermissionGroup.storage);
    if (permissionStatusStorage == handler.PermissionStatus.denied) {
      Map<handler.PermissionGroup, handler.PermissionStatus> permissions =
          await handler.PermissionHandler()
              .requestPermissions([handler.PermissionGroup.storage]);
      if (Platform.isAndroid) {
        dirLoc = "/sdcard/Adira PDF";
        try {
          FileUtils.mkdir([dirLoc]);
          Timer(Duration(seconds: 2), () {
            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (context) => LoginPage()));
          });
        } catch (e) {}
      } else {
        dirLoc = (await getApplicationDocumentsDirectory()).path;
      }
    } else {
      Timer(Duration(seconds: 2), () {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => LoginPage()));
      });
      dirLoc = "/sdcard/Adira PDF";
    }
  }
}
