import 'package:adira_partner/screens/chat/chat.dart';
import 'package:adira_partner/screens/draft/draft.dart';
import 'package:adira_partner/screens/login/login.dart';
import 'package:adira_partner/screens/profile/profile.dart';
import 'package:adira_partner/screens/reschedule/reschedule.dart';
import 'package:adira_partner/screens/submit_order/submit_order.dart';
import 'package:adira_partner/screens/tracking_order/tracking_order.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'constants/constanta.dart';
import 'constants/size_config.dart';
import 'package:adira_partner/db_helper/database_helper.dart';
import 'provider/login_api_provider.dart';

class HomePage extends StatefulWidget {
  final int dif;
  final int expiredToken;

  const HomePage({Key key, this.dif, this.expiredToken}) : super(key: key);

//  final drawerItems = [
//    DrawerItem("Submit Order",Icons.home),
//    DrawerItem("Tracking Order",Icons.local_shipping),
//    DrawerItem("Draft",Icons.insert_drive_file),
//    DrawerItem("Signout",Icons.exit_to_app)
//  ];
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  DbHelper _dbHelper = DbHelper();
  var _checkSession = false;
  Screen size;
  LoginApiProvider _loginApiProvider = LoginApiProvider();
  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _menuProfile = true;
  bool _menuSubmitOrder = true;
  bool _menuTrackingOrder = true;
  bool _menuDraft = true;
  bool _menuChat = false;
  bool _menuReschedule = true;

  List _menu = ['Profile', 'Submit Order'];

  @override
  Widget build(BuildContext context) {
    size = Screen(MediaQuery.of(context).size);
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        //backgroundColor: kPrimaryColor,
        title: Image.asset('assets/img/logo_adira.png', scale: 2),
        centerTitle: false,
        iconTheme: new IconThemeData(color: Colors.black),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[kPrimaryColor, Colors.grey[350]])),
        ),
        automaticallyImplyLeading: false,
        actions: <Widget>[
          Container(
            margin: EdgeInsets.only(right: size.wp(2)),
            child: PopupMenuButton<String>(
                onSelected: (String value) {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => LoginPage()));
                },
                child: Icon(Icons.more_vert),
                itemBuilder: (context) {
                  return Constant.choices.map((String choice) {
                    return PopupMenuItem(
                      value: choice,
                      child: Text(choice),
                    );
                  }).toList();
                }),
          )
        ],
      ),
      body: Stack(
        children: <Widget>[
          ListView(
            padding: EdgeInsets.only(top: 16, bottom: 16, left: 8, right: 8),
            scrollDirection: Axis.vertical,
            children: <Widget>[
              _menuProfile == true ? Profile() : Container(),
              _menuSubmitOrder == true ? SubmitOrder() : Container(),
              _menuTrackingOrder == true ? TrackingOrder() : Container(),
              _menuDraft == true ? Draft() : Container(),
              _menuChat == true ? Chat() : Container(),
              _menuReschedule == true ? Reschedule() : Container(),
            ],
          )
        ],
      ),
    );
  }

  _checkSessionFromSubmitOrderButton() async {
    setState(() {
      _checkSession = true;
    });
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _dateNow = DateTime.now();
    DateFormat _formatter = DateFormat("yyyy-MM-dd");
    String _dateNowText = _formatter.format(_dateNow);
    DateTime _dateNowFinal = DateTime.parse(_dateNowText);
    var _result = await _dbHelper.getDateLogin();
    var _dateLogout = _result[0]['logout_date'];
    var _dateLogoutFinal = DateTime.parse(_dateLogout);
    Duration _difference = _dateNowFinal.difference(_dateLogoutFinal);
    if (_difference.inDays == 0) {
      _dbHelper.deleteDateLogin();
      _preferences.clear();
      setState(() {
        _checkSession = false;
      });
      Navigator.pushAndRemoveUntil(
          context,
          PageRouteBuilder(pageBuilder: (BuildContext context,
              Animation animation, Animation secondaryAnimation) {
            return LoginPage();
          }, transitionsBuilder: (BuildContext context,
              Animation<double> animation,
              Animation<double> secondaryAnimation,
              Widget child) {
            return new SlideTransition(
              position: new Tween<Offset>(
                begin: const Offset(1.0, 0.0),
                end: Offset.zero,
              ).animate(animation),
              child: child,
            );
          }),
          (Route route) => false);
    } else {
      setState(() {
        _checkSession = false;
      });
      // Navigator.of(context).push(
      //     MaterialPageRoute(builder: (context) => FormTransaksionalPage()));
    }
  }

  _checkSessionFromTrackingButton() async {
    setState(() {
      _checkSession = true;
    });
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _dateNow = DateTime.now();
    DateFormat _formatter = DateFormat("yyyy-MM-dd");
    String _dateNowText = _formatter.format(_dateNow);
    DateTime _dateNowFinal = DateTime.parse(_dateNowText);
    var _result = await _dbHelper.getDateLogin();
    var _dateLogout = _result[0]['logout_date'];
    var _dateLogoutFinal = DateTime.parse(_dateLogout);
    Duration _difference = _dateNowFinal.difference(_dateLogoutFinal);
    if (_difference.inDays == 0) {
      _dbHelper.deleteDateLogin();
      _preferences.clear();
      setState(() {
        _checkSession = false;
      });
      Navigator.pushAndRemoveUntil(
          context,
          PageRouteBuilder(pageBuilder: (BuildContext context,
              Animation animation, Animation secondaryAnimation) {
            return LoginPage();
          }, transitionsBuilder: (BuildContext context,
              Animation<double> animation,
              Animation<double> secondaryAnimation,
              Widget child) {
            return new SlideTransition(
              position: new Tween<Offset>(
                begin: const Offset(1.0, 0.0),
                end: Offset.zero,
              ).animate(animation),
              child: child,
            );
          }),
          (Route route) => false);
    } else {
      setState(() {
        _checkSession = false;
      });
      // Navigator.of(context)
      //     .push(MaterialPageRoute(builder: (context) => TrackingOrder()));
    }
  }

  _checkSessionFromDraftButton() async {
    setState(() {
      _checkSession = true;
    });
    SharedPreferences _preferences = await SharedPreferences.getInstance();
    var _dateNow = DateTime.now();
    DateFormat _formatter = DateFormat("yyyy-MM-dd");
    String _dateNowText = _formatter.format(_dateNow);
    DateTime _dateNowFinal = DateTime.parse(_dateNowText);
    var _result = await _dbHelper.getDateLogin();
    var _dateLogout = _result[0]['logout_date'];
    var _dateLogoutFinal = DateTime.parse(_dateLogout);
    Duration _difference = _dateNowFinal.difference(_dateLogoutFinal);
    if (_difference.inDays == 0) {
      _dbHelper.deleteDateLogin();
      _preferences.clear();
      setState(() {
        _checkSession = false;
      });
      Navigator.pushAndRemoveUntil(
          context,
          PageRouteBuilder(pageBuilder: (BuildContext context,
              Animation animation, Animation secondaryAnimation) {
            return LoginPage();
          }, transitionsBuilder: (BuildContext context,
              Animation<double> animation,
              Animation<double> secondaryAnimation,
              Widget child) {
            return new SlideTransition(
              position: new Tween<Offset>(
                begin: const Offset(1.0, 0.0),
                end: Offset.zero,
              ).animate(animation),
              child: child,
            );
          }),
          (Route route) => false);
    } else {
      setState(() {
        _checkSession = false;
      });
      // Navigator.of(context)
      //     .push(MaterialPageRoute(builder: (context) => DraftPage()));
    }
  }

  _logout() async {
    try {
      print("testLogout");
      var _result = await _loginApiProvider.logout();
      if (_result == "200") {
        var _getAllData = await _dbHelper.getToken();
        var _result = await _dbHelper.updateTokenLogout(_getAllData[0]['id']);
        if (_result == 1) {
          Navigator.pushAndRemoveUntil(
              context,
              PageRouteBuilder(pageBuilder: (BuildContext context,
                  Animation animation, Animation secondaryAnimation) {
                return LoginPage();
              }, transitionsBuilder: (BuildContext context,
                  Animation<double> animation,
                  Animation<double> secondaryAnimation,
                  Widget child) {
                return new SlideTransition(
                  position: new Tween<Offset>(
                    begin: const Offset(1.0, 0.0),
                    end: Offset.zero,
                  ).animate(animation),
                  child: child,
                );
              }),
              (Route route) => false);
        }
      }
    } catch (e) {
      _showSnackBar(e);
    }
  }

  void _showSnackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text(text), behavior: SnackBarBehavior.floating));
  }

  @override
  void initState() {
    super.initState();
//    if(widget.dif != null){
//      _checkSessionFromTrackingButton();
//    }
    Future.delayed(const Duration(milliseconds: 1000), () {
      _checkTimeTokenExpired();
    });
  }

  _checkTimeTokenExpired() {
    if (widget.expiredToken != null) {
      var date =
          new DateTime.fromMillisecondsSinceEpoch(widget.expiredToken * 1000);
      var _now = DateTime.now();
      int _difference = date.difference(_now).inMinutes;
      if (_difference < 15) {
        _showDialogWarningTimeToken(_difference);
      }
    }
  }

  _showDialogWarningTimeToken(int minute) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Warning", style: TextStyle(fontFamily: "NunitoSans")),
            content: Text(
                "Sesi anda akan berakhir $minute menit lagi,\nSilahkan logout dan login kembali"),
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("CLOSE",
                      style: TextStyle(
                          fontFamily: "NunitoSans", color: kPrimaryColor))),
            ],
          );
        });
  }
}

class Reschedule extends StatelessWidget {
  const Reschedule({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => ReschedulePage()));
      },
      child: Card(
        elevation: 5.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Container(
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 4,
                child: Container(
                  margin: EdgeInsets.only(top: 8, bottom: 8),
                  child: ClipRRect(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(8),
                          topLeft: Radius.circular(8)),
                      child: Image.asset("assets/img/tracking_order.webp",
                          height: 97, width: 117)),
                ),
              ),
              Expanded(
                  flex: 4,
                  child: Text("Reschedule",
                      style: TextStyle(
                          fontFamily: "NunitoSansSemiBold", fontSize: 24)))
            ],
          ),
        ),
      ),
    );
  }
}

class Chat extends StatelessWidget {
  const Chat({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => ChatPage()));
      },
      child: Card(
        elevation: 5.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Container(
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 4,
                child: Container(
                  margin: EdgeInsets.only(top: 8, bottom: 8),
                  child: ClipRRect(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(8),
                          topLeft: Radius.circular(8)),
                      child: Image.asset("assets/img/smartphone.webp",
                          height: 97, width: 117)),
                ),
              ),
              Expanded(
                  flex: 4,
                  child: Text("Chat SO D/M",
                      style: TextStyle(
                          fontFamily: "NunitoSansSemiBold", fontSize: 24)))
            ],
          ),
        ),
      ),
    );
    SizedBox(height: 4);
  }
}

class Draft extends StatelessWidget {
  const Draft({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => DraftPage()));
      },
      child: Card(
        elevation: 5.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Container(
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 4,
                child: Container(
                  margin: EdgeInsets.only(top: 8, bottom: 8),
                  child: ClipRRect(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(8),
                          topLeft: Radius.circular(8)),
                      child: Image.asset("assets/img/draft.webp",
                          height: 97, width: 117)),
                ),
              ),
              Expanded(
                  flex: 4,
                  child: Text("Draft",
                      style: TextStyle(
                          fontFamily: "NunitoSansSemiBold", fontSize: 24)))
            ],
          ),
        ),
      ),
    );
    SizedBox(height: 4);
  }
}

class TrackingOrder extends StatelessWidget {
  const TrackingOrder({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => TrackingOrderPage()));
      },
      child: Card(
        elevation: 5.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Container(
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 4,
                child: Container(
                  margin: EdgeInsets.only(top: 8, bottom: 8),
                  child: ClipRRect(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(8),
                          topLeft: Radius.circular(8)),
                      child: Image.asset("assets/img/tracking_order.webp",
                          height: 97, width: 117)),
                ),
              ),
              Expanded(
                  flex: 4,
                  child: Text("Tracking Order",
                      style: TextStyle(
                          fontFamily: "NunitoSansSemiBold", fontSize: 24)))
            ],
          ),
        ),
      ),
    );
    SizedBox(height: 4);
  }
}

class SubmitOrder extends StatelessWidget {
  const SubmitOrder({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => SubmitOrderPage()));
      },
      child: Card(
        elevation: 5.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Container(
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 4,
                child: Container(
                  margin: EdgeInsets.only(top: 8, bottom: 8),
                  child: ClipRRect(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(8),
                          topLeft: Radius.circular(8)),
                      child: Image.asset("assets/img/submit_order.webp",
                          height: 97, width: 117)),
                ),
              ),
              Expanded(
                  flex: 4,
                  child: Text("Submit Order",
                      style: TextStyle(
                          fontFamily: "NunitoSansSemiBold", fontSize: 24)))
            ],
          ),
        ),
      ),
    );
    SizedBox(height: 4);
  }
}

class Profile extends StatelessWidget {
  const Profile({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => ProfilePage()));
      },
      child: Card(
        elevation: 5.0,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        child: Container(
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 4,
                child: Container(
                  margin: EdgeInsets.only(top: 8, bottom: 8),
                  child: ClipRRect(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(8),
                          topLeft: Radius.circular(8)),
                      child: Image.asset(
                        "assets/img/profile.webp",
                        height: 97,
                        width: 117,
                      )),
                ),
              ),
              Expanded(
                  flex: 4,
                  child: Text("Profile",
                      style: TextStyle(
                          fontFamily: "NunitoSansSemiBold", fontSize: 24)))
            ],
          ),
        ),
      ),
    );
    SizedBox(height: 4);
  }
}
