import 'dart:convert';
import 'dart:io';

import 'package:adira_partner/constants/url.dart';
import 'package:adira_partner/db_helper/database_helper.dart';
import 'package:http/http.dart' show Client;
import 'package:http/io_client.dart';

class GetDataKecamatanApiProvider {
  Client _client = Client();
  DbHelper _dbHelper = DbHelper();
  Future<Map> getAllKecamatan(String kecamatan) async {
    //List _resulTokenFromSQLIte = await _dbHelper.getToken();
    //rint("${_resulTokenFromSQLIte[0]['token']}");
    final ioc = new HttpClient();
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);

    try {
      final _response = await http.get(
          "https://af-extwsuat.adira.co.id/ad1gateapi/api/Submit/GetKecamatan?search=KEBON JERUK");
      final _data = jsonDecode(_response.body);
      if (_response.statusCode == 200) {
        if (_data['Status'] == 0) {
          print(_data);
          var _listAllKecamatan = [];
          for (var i = 0; i < _data['Data'].length; i++) {
            _listAllKecamatan.add(_data['Data'][i]);
          }
          var _result = {"status": true, "listAllKecamatan": _listAllKecamatan};
          print(_result);
          return _result;
        } else {
          var _dataError = {"error_description": _data['Message']};
          var _result = {"status": false, "data": _dataError};
          return _result;
        }
      } else {
        var _result = {"status": false, "data": _data};
        return _result;
      }
    } catch (e) {
      var _data = {"error_description": e.toString()};
      var _result = {"status": false, "data": _data};
      return _result;
    }
  }
}
