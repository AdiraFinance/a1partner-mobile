import 'package:adira_partner/constants/constanta.dart';
import 'package:adira_partner/constants/currency.dart';
import 'package:adira_partner/constants/size_config.dart';
import 'package:adira_partner/db_helper/database_helper.dart';
import 'package:adira_partner/model/pekerjaan_model.dart';
import 'package:adira_partner/provider/generate_date_api_provider.dart';
import 'package:adira_partner/provider/get_data_tracking_api_provider.dart';
import 'package:adira_partner/provider/submit_order_api_provider.dart';
import 'package:adira_partner/screens/submit_order/search_kecamatan.dart';
import 'package:adira_partner/screens/submit_order/search_model_kendaraan.dart';
import 'package:adira_partner/services/map_location.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';

import 'package:permission_handler/permission_handler.dart' as handler;

class SubmitOrderPage extends StatefulWidget {
  @override
  _SubmitOrderPageState createState() => _SubmitOrderPageState();
}

class _SubmitOrderPageState extends State<SubmitOrderPage> {
  Screen size;
  int _currentStep = 0;
  VoidCallback _onStepContinue, _onStepCancel;
  bool _hasKtp = false;
  Gender _gender;
  int _radioValue = 0;
  int _radioCustTypeValue = 0;
  bool _enableAlamatKTP = false;
  bool _loadData = false;
  DbHelper _dbHelper = DbHelper();
  List<ResultPekerjaan> _listResultPekerjaan = [];
  ResultPekerjaan _resultPekerjaan;
  Branch _pilihanCabang;
  PemilihanMktDedicated _pilihanMktDedicated;
  PemilihanCaraSurvey _pemilihanCaraSurvey;
  List<PemilihanCaraSurvey> _listPemilihanCaraSurvey = [
    PemilihanCaraSurvey(5, "Sales Officer"),
    // PemilihanCaraSurvey(4, "Submit To Action"),
  ];

  var _isProcessCheckVoucher = false;
  var _isUseVoucher = " ";

  final _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _key = new GlobalKey<FormState>();
  var _processSendData = false;
  var _enableAutofilled = true;
  bool _validate = false;
  var _autoValidate = false;
  var _autoValidate2 = false;
  var _autoValidate3 = false;
  var _autoValidate4 = false;
  var _validatesubmit = false;
  var _validatefinish = false;
  var _enableSearchModel = false;

  var nomerKTP,
      namaLengkap,
      namaPanggilan,
      tempatLahir,
      birthDate,
      gender,
      statusPernikahan,
      alamatSurvey,
      kecamatan,
      kelurahan,
      kodePos,
      alamatKTP,
      kecamatanKTP,
      kelurahanKTP,
      kodePosKTP,
      tlpnRumah,
      noHp,
      kodeArea,
      thnLahir,
      blnLahir,
      tglLahir,
      _timeSurvey,
      _dateSurvey;

  var idKelurahan, idKecamatan, idKota, idKotaProvinsi;
  var idKelurahanKTP, idKecamatanKTP, idKotaKTP, idKotaProvinsiKTP;

  final _controllerNomerKTP = new TextEditingController();
  final _controllerNamaLengkap = new TextEditingController();
  final _controllerNamaPanggilan = new TextEditingController();
  final _controllerTempatLahir = new TextEditingController();
  final _controllerAlamatSurvey = TextEditingController();
  final _controllerRT = TextEditingController();
  final _controllerRW = TextEditingController();
  final _controllerKecamatan = TextEditingController();
  final _controllerKelurahan = TextEditingController();
  final _controllerKodePos = TextEditingController();
  final _controllerAlamatKTP = TextEditingController();
  final _controllerRtKtp = TextEditingController();
  final _controllerRwKtp = TextEditingController();
  final _controllerKecamatanKTP = TextEditingController();
  final _controllerKelurahanKTP = TextEditingController();
  final _controllerKodePosKTP = TextEditingController();
  final _controllerKodeArea = TextEditingController();
  final _controllerTlpnRumah = TextEditingController();
  final _controllerKodeNegara = TextEditingController();
  final _controllerNoHp = TextEditingController();
  final _controllerRedeemVoucher = TextEditingController();

  final _controllerThnlahir = new TextEditingController();
  final _controllerBlnlahir = new TextEditingController();
  final _controllerTgllahir = new TextEditingController();

  final _controllerDealerCabang = new TextEditingController();
  final _controllerModelKendaraan = new TextEditingController();
  final _controllerTipeKendaraan = new TextEditingController();
  final _controllerMerkKendaraan = new TextEditingController();
  final _controllerThnProduksi = new TextEditingController();
  final _controllerNamaPadaBPKB = new TextEditingController();

  var dealerCabang,
      jenisKendaraanDipilih,
      modelKendaraan,
      tipeKendaraan,
      merkKendaraan,
      thnProduksi,
      namaPadaBPKB;

  final FocusNode _focusNodeModel = FocusNode();
  final FocusNode _focusNodeTipe = FocusNode();
  final FocusNode _focusNodeMerk = FocusNode();
  final FocusNode _focusNodeThnProd = FocusNode();
  final FocusNode _focusNodeNamaPadaBPKB = FocusNode();

  final FocusNode _focusNodeNoKTP = FocusNode();
  final FocusNode _focusNodeNamaLengkap = FocusNode();
  final FocusNode _focusNodeNamaPanggilan = FocusNode();
  final FocusNode _focusNodeTempatLahir = FocusNode();
  final FocusNode _focusNodeAlamatSurvey = FocusNode();
  final FocusNode _focusNodeRT = FocusNode();
  final FocusNode _focusNodeRW = FocusNode();
  final FocusNode _focusNodeRtKtp = FocusNode();
  final FocusNode _focusNodeRwKtp = FocusNode();
  final FocusNode _focusNodeKecamatan = FocusNode();
  final FocusNode _focusNodeKelurahan = FocusNode();
  final FocusNode _focusNodeKodePos = FocusNode();
  final FocusNode _focusNodeAlamatKTP = FocusNode();
  final FocusNode _focusNodeKecamatanKTP = FocusNode();
  final FocusNode _focusNodeKelurahanKTP = FocusNode();
  final FocusNode _focusNodeKodePosKTP = FocusNode();
  final FocusNode _focusNodeKodeArea = FocusNode();
  final FocusNode _focusNodeTlpnRumah = FocusNode();
  final FocusNode _focusNodeKodeNegara = FocusNode();
  final FocusNode _focusNodeNoHp = FocusNode();
  final _focusNodeRedeemVoucher = FocusNode();

  final _controllerJenisPembiyaan = new TextEditingController();
  final _controllerOTR = new TextEditingController();
  final _controllerAngsuran = new TextEditingController();
  final _controllerDP = TextEditingController();

  var otr, _angsuran, _tenor;
  String jenisPembiayaanDipilih;
  var _validateJenisPembiayaan = false;
  final FocusNode _focusNodeAngsuran = FocusNode();
  final FocusNode _focusNodeOTR = FocusNode();
  final FocusNode _focusNodeDp = FocusNode();

  final _controllerNamaLengkapPasangan = new TextEditingController();
  final _controllerNamaGadisIbuKandung = new TextEditingController();
  final _controllerCatatanDealer = new TextEditingController();
  final _controllerLocation = TextEditingController();

  final FocusNode _focusNodeNamaLengkapPasangan = FocusNode();
  final FocusNode _focusNodeNamaIbuKandung = FocusNode();
  final FocusNode _focusNodeCatatanDealer = FocusNode();

  var namaLengkapPasangan, namaGadisIbuKandung, catatanDealer;
  String _surveyDateSelected, _surveyMenitSelected;
  var _surveyJamSelected;

  var _timeSekarang = TimeOfDay.now();
  Location _locationService = new Location();
  bool _permission = false;
  handler.PermissionStatus permissionStatusLocation;
  var _latitude, _longitude;

  SubmitOrderForm _submitOrderForm;
  List _listDealerCabang = [];
  GetDataTrackingApiProvider _getDataTrackingApiProvider =
      GetDataTrackingApiProvider();
  List<Branch> _listBranch = [];
  List<PemilihanMktDedicated> _listMkt = [];

  List<GlobalKey<FormState>> formKeys = [
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>(),
    GlobalKey<FormState>()
  ];

  List<Gender> listGender = <Gender>[
    Gender("01", "Pria"),
    Gender("02", "Wanita")
  ];

  var listStatusPernikahan = [
    {
      "id": "01",
      "statusKawin": "Kawin",
    },
    {
      "id": "02",
      "statusKawin": "Single",
    },
    {
      "id": "03",
      "statusKawin": "Duda/Janda Tanpa Anak",
    },
    {
      "id": "04",
      "statusKawin": "Duda/Janda Dgn Anak",
    },
    {
      "id": "99",
      "statusKawin": "Migrasi",
    }
  ];

  JenisKendaraan _jenisKendaraan;
  List<JenisKendaraan> listJenisKendaraan = <JenisKendaraan>[
    JenisKendaraan("001", "Motor Baru"),
    JenisKendaraan("002", "Motor Bekas"),
    JenisKendaraan("003", "Mobil Baru"),
    JenisKendaraan("004", "Mobil Bekas"),
    JenisKendaraan("005", "Durable")
  ];

  var _listTahunKendaraan = ["Pilih jenis kendaraan terlebih dahulu"];
  List<String> _listTahunKendaraanBaru = [];
  List<String> _listTahunKendaraanBekas = [];
  var _tahunKendaraanDipilih,
      _tahunKendaraanDipilihBekas,
      _tahunKendaraanDipilihBaru;

  _addTahunKendaraan() {
    print(_jenisKendaraan.id);
    var _tahunKendaraan = DateTime.now().year;

    if (_jenisKendaraan.id == "001" || _jenisKendaraan.id == "003") {
      setState(() {
        _listTahunKendaraanBaru.clear();
        _listTahunKendaraanBaru.add((_tahunKendaraan - 1).toString());
        _listTahunKendaraanBaru.add(_tahunKendaraan.toString());
        _listTahunKendaraanBaru.add((_tahunKendaraan + 1).toString());
        _tahunKendaraanDipilihBaru = _listTahunKendaraanBaru[1];
      });
      print(_listTahunKendaraanBaru.length);
    } else {
      setState(() {
        _listTahunKendaraanBekas.clear();
      });
      for (int i = 0; i <= 10; i++) {
        _listTahunKendaraanBekas.add((_tahunKendaraan - i).toString());
      }
      setState(() {
        _tahunKendaraanDipilihBekas = _listTahunKendaraanBekas[0];
      });

      // _tahunKendaraanDipilihBekas,

    }
  }

  String _textKota, _textProv;
  setValueKecamatan(Map value) {
    print("cek kecamatan dkk $value");
    setState(() {
      _controllerKecamatan.text = value['PARA_KECAMATAN_DESC'];
      _controllerKelurahan.text = value['PARA_KELURAHAN_DESC'];
      _controllerKodePos.text = value['PARA_KELURAHAN_ZIP_CODE'];
      _textKota = value['KAB_KOT_DESC'];
      _textProv = value['PROVINSI_DESC'];
      idKecamatan = value['PARA_KELURAHAN_ID_KEC'];
      idKelurahan = value['PARA_KELURAHAN_ID'];
      idKota = value['KAB_KOT_ID'];
      idKotaProvinsi = value['KAB_KOT_ID_PROV'];
    });
  }

  String _textKotaKTP, _textProvKTP;
  setValueKecamatanKTP(Map value) {
    print("cek value KTP $value");
    setState(() {
      _controllerKecamatanKTP.text = value['PARA_KECAMATAN_DESC'];
      _controllerKelurahanKTP.text = value['PARA_KELURAHAN_DESC'];
      _controllerKodePosKTP.text = value['PARA_KELURAHAN_ZIP_CODE'];
      _textKotaKTP = value['KAB_KOT_DESC'];
      _textProvKTP = value['PROVINSI_DESC'];
      idKecamatanKTP = value['PARA_KELURAHAN_ID_KEC'];
      idKelurahanKTP = value['PARA_KELURAHAN_ID'];
      idKotaKTP = value['KAB_KOT_ID'];
      idKotaProvinsiKTP = value['KAB_KOT_ID_PROV'];
    });
  }

  _getPekerjaan() async {
    var _result = await _dbHelper.getPekerjaan();
    print("cek _result ${_result.resultListPekerjaan[0].occupationCode}");
    for (var i = 0; i < _result.resultListPekerjaan.length; i++) {
      _listResultPekerjaan.add(_result.resultListPekerjaan[i]);
    }
    setState(() {
      _resultPekerjaan = _listResultPekerjaan[2];
    });
  }

  cekDateTime() {
    var now = new DateTime.now();
    var formatter = new DateFormat('dd-MMM-yyyy');
    String formatted = formatter.format(now);
    setState(() {
      _dateSurvey = formatted;
    });

    String s = TimeOfDay.now().toString();
    String _format = s.split("(")[1];
    String _format2 = _format.split(")")[0];
    setState(() {
      _timeSurvey = _format2;
    });
  }

  JenisPembiayaan _jenisPembiayaan;
  List<JenisPembiayaan> listJenisPembiayaan = <JenisPembiayaan>[
    JenisPembiayaan(1, "Konvensional"),
    JenisPembiayaan(2, "Syariah"),
  ];

  ProdukPembiayaan _produkPembiayaan;
  List<ProdukPembiayaan> listProdukPembiayaan = <ProdukPembiayaan>[
    ProdukPembiayaan(1, "Reguler"),
    ProdukPembiayaan(2, "Smart"),
  ];

  JenisAsuransi _jenisAsuransi;
  List<JenisAsuransi> listJenisAsuransi = <JenisAsuransi>[
    JenisAsuransi(1, "Asuransi 1"),
    JenisAsuransi(2, "Asuransi 2"),
  ];

  FormDataNasabah _listFormDataNasabah;
  List<FormDataNasabah> listFormDataNasabah = <FormDataNasabah>[
    FormDataNasabah("001", "No KTP", true),
    FormDataNasabah("002", "Redeem Voucher", true),
    FormDataNasabah("003", "Nama Lengkap", true),
    FormDataNasabah("004", "Tempat Lahir", true),
    FormDataNasabah("005", "Tanggal Lahir", true),
    FormDataNasabah("006", "Gender", true),
    FormDataNasabah("007", "Status Pernikahan", true),
    FormDataNasabah("008", "Alamat Survey", true),
    FormDataNasabah("009", "RT/RW", true),
    FormDataNasabah("010", "Kecamatan", true),
    FormDataNasabah("011", "Kelurahan", true),
    FormDataNasabah("012", "Kode Pos", true),
    FormDataNasabah("013", "Alamat KTP", true),
    FormDataNasabah("014", "Telepon Rumah", true),
    FormDataNasabah("015", "No HP", true),
    FormDataNasabah("016", "Pekerjaan", true),
    FormDataNasabah("017", "Upload Foto KTP", true),
  ];

  var _listPilihanTenor = [
    "6",
    "11",
    "12",
    "17",
    "18",
    "21",
    "23",
    "24",
    "25",
    "29",
    "30",
    "35",
    "36",
    "37",
    "39",
    "41",
    "42",
    "47",
    "48",
    "54",
    "59",
    "60",
    "61",
    "63",
    "65",
    "66",
    "67",
    "69",
    "72",
    "78",
    "84",
    "90"
  ];

  @override
  void initState() {
    super.initState();
    _gender = listGender[0];
    _genenerateDateApiProvider = GenerateDateApiProvider();
    // _checkValidRedeemVoucher = CheckValidRedeemVoucher();
    _submitOrderForm = SubmitOrderForm();
    // _getSourceReffID = GetSourceReffID();
    cekDateTime();
    processDate();
    _getPekerjaan();
    // checkPermissionLocation();

    setState(() {
      _gender = listGender[0];
      statusPernikahan = "02";
      _controllerKodeArea.text = "021";
      _controllerKodeNegara.text = "62";

      _jenisPembiayaan = listJenisPembiayaan[0];
      _pemilihanCaraSurvey = _listPemilihanCaraSurvey[0];
      _tenor = _listPilihanTenor[11];
    });
  }

  @override
  Widget build(BuildContext context) {
    size = Screen(MediaQuery.of(context).size);
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: Text("Tambah Pengajuan",
            style: TextStyle(fontFamily: "NunitoSans", color: Colors.black)),
        //backgroundColor: kPrimaryColor,
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[kPrimaryColor, Colors.grey[350]])),
        ),
        automaticallyImplyLeading: false,
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              if (_hasKtp) {
                // _onWillPop();
              } else {
                _backToHomePage();
              }
            }),
      ),
      body: Form(
        // onWillPop: _onWillPop,
        key: _key,
        child: Theme(
          data: ThemeData(primaryColor: const Color(0xff25AE88)),
          child: _processSendData
              ? Center(
                  child: CircularProgressIndicator(
                      valueColor:
                          new AlwaysStoppedAnimation<Color>(kPrimaryColor)))
              : Stepper(
                  steps: <Step>[
                    Step(
                      title: Text(''),
                      content: Form(
                          child: _dataNasabahStepWidget(), key: formKeys[0]),
                      isActive: true,
                      state: _currentStep >= 0
                          ? StepState.complete
                          : StepState.disabled,
                    ),
                    Step(
                      title: Text(''),
                      content: Form(
                        child: _dataKendaraanStepWidget(),
                        key: formKeys[1],
                      ),
                      isActive: _currentStep >= 1,
                      state: _currentStep >= 1
                          ? StepState.complete
                          : StepState.disabled,
                    ),
                    Step(
                      title: Text(''),
                      content:
                          Form(child: _dataPengajuanKredit(), key: formKeys[2]),
                      isActive: _currentStep >= 2,
                      state: _currentStep >= 2
                          ? StepState.complete
                          : StepState.disabled,
                    ),
                    Step(
                      title: Text(''),
                      content: Form(
                          child: _loadData
                              ? Container(
                                  margin: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.height /
                                          3.3),
                                  child: Center(
                                      child: CircularProgressIndicator(
                                          valueColor:
                                              new AlwaysStoppedAnimation<Color>(
                                                  kPrimaryColor))))
                              : _dataLain(),
                          key: formKeys[3]),
                      isActive: _currentStep >= 3,
                      state: _currentStep >= 3
                          ? StepState.complete
                          : StepState.disabled,
                    ),
                  ],
                  type: StepperType.horizontal,
                  currentStep: _currentStep,
                  onStepTapped: null,
                  controlsBuilder: (BuildContext context,
                      {VoidCallback onStepContinue,
                      VoidCallback onStepCancel}) {
                    _onStepContinue = onStepContinue;
                    _onStepCancel = onStepCancel;
                    return SizedBox(
                      width: 0.0,
                      height: 0.0,
                    );
                  },
                  onStepCancel: _currentStep > 0
                      ? () => setState(() => _currentStep -= 1)
                      : null,
                  onStepContinue: () {
                    setState(() => _currentStep += 1);
                  },
                ),
        ),
      ),
      bottomNavigationBar: _processSendData
          ? null
          : BottomAppBar(
              elevation: 0.0,
              child: Container(
                margin: EdgeInsets.only(
                    left: size.wp(2.5),
                    right: size.wp(2.5),
                    bottom: size.hp(1),
                    top: size.hp(1)),
                child: _currentStep == 0
                    ? RaisedButton(
                        padding: EdgeInsets.only(
                            top: size.hp(1.5), bottom: size.hp(1.5)),
                        onPressed: () {
                          //check();
                          // _saveToDraftpage1();

                          _onStepContinue();
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(8.0)),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text('Selanjutnya',
                                style: TextStyle(
                                    fontFamily: "NunitoSans",
                                    color: Colors.black))
                          ],
                        ),
                        color: kPrimaryColor,
                      )
                    : _currentStep < 3
                        ? Row(children: <Widget>[
                            Expanded(
                              child: RaisedButton(
                                padding: EdgeInsets.only(
                                    top: size.hp(1.5), bottom: size.hp(1.5)),
                                shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(8.0),
                                ),
                                onPressed: () {
                                  _onStepCancel();
                                },
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      'Kembali',
                                      style: TextStyle(
                                          fontFamily: "NunitoSans",
                                          color: Colors.black),
                                    ),
                                  ],
                                ),
                                color: const Color(0xffe6e6e6),
                              ),
                              flex: 5,
                            ),
                            SizedBox(width: size.wp(2)),
                            Expanded(
                              child: RaisedButton(
                                padding: EdgeInsets.only(
                                    top: size.hp(1.5), bottom: size.hp(1.5)),
                                onPressed: () {
                                  _onStepContinue();
                                  // check();
                                  // _saveToDraftpage2();
                                  if (_currentStep > 2) {
                                    //_getGenerateDate();
                                  }
                                },
                                shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(8.0),
                                ),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text('Selanjutnya',
                                        style: TextStyle(
                                            fontFamily: "NunitoSans",
                                            color: Colors.black))
                                  ],
                                ),
                                color: kPrimaryColor,
                              ),
                              flex: 5,
                            ),
                          ])
                        : Row(children: <Widget>[
                            Expanded(
                              child: RaisedButton(
                                padding: EdgeInsets.only(
                                    top: size.hp(1.5), bottom: size.hp(1.5)),
                                shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(8.0),
                                ),
                                onPressed: _onStepCancel,
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      'Kembali',
                                      style: TextStyle(
                                          fontFamily: "NunitoSans",
                                          color: Colors.black),
                                    ),
                                  ],
                                ),
                                color: const Color(0xffe6e6e6),
                              ),
                              flex: 5,
                            ),
                            SizedBox(width: size.wp(2)),
                            Expanded(
                              child: RaisedButton(
                                padding: EdgeInsets.only(
                                    top: size.hp(1.5), bottom: size.hp(1.5)),
                                onPressed: _validatefinish == false
                                    ? null
                                    : () {
                                        //check();
                                      },
                                shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(8.0),
                                ),
                                child: Row(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text('Selesai',
                                        style: TextStyle(
                                            fontFamily: "NunitoSans",
                                            color: Colors.black))
                                  ],
                                ),
                                color: kPrimaryColor,
                              ),
                              flex: 5,
                            ),
                          ]),
              )),
    );
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  Widget _dataNasabahStepWidget() {
    return Theme(
      data: ThemeData(primaryColor: Colors.black),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Text(
              'Data Nasabah',
              style: TextStyle(fontFamily: "NunitoSansSemiBold", fontSize: 18),
            ),
          ),
          SizedBox(height: size.hp(2)),
          Container(
            child: Text("Tipe Nasabah",
                style: TextStyle(fontFamily: "NunitoSans")),
          ),
          Row(
            children: <Widget>[
              Radio(
                value: 0,
                groupValue: _radioCustTypeValue,
                onChanged: _handleRadioCustTypeValueChange,
                activeColor: const Color(0xff25AE88),
              ),
              Text("Personal", style: TextStyle(fontFamily: "NunitoSans")),
              Radio(
                value: 1,
                groupValue: _radioCustTypeValue,
                onChanged: _handleRadioCustTypeValueChange,
                activeColor: const Color(0xff25AE88),
              ),
              Text("Company", style: TextStyle(fontFamily: "NunitoSans")),
            ],
          ),
          _radioCustTypeValue == 0
              ? dataNasabahPersonal()
              : dataNasabahCompany(),
          SizedBox(height: size.hp(2))
        ],
      ),
    );
  }

  Column dataNasabahPersonal() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
        Widget>[
      Row(
        children: [
          Expanded(
              flex: 6,
              child: TextFormField(
                autovalidate: _autoValidate3,
                controller: _controllerNomerKTP,
                decoration: new InputDecoration(
                  labelText: 'Nomor KTP *',
                  labelStyle: TextStyle(fontFamily: "NunitoSans"),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8)),
                ),
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.number,
                focusNode: _focusNodeNoKTP,
                onFieldSubmitted: (term) {
                  _fieldFocusChange(
                      context, _focusNodeNoKTP, _focusNodeNamaLengkap);
                },
                enabled: _enableAutofilled,
                validator: (e) {
                  if (e.isEmpty) {
                    return "Tidak boleh kosong";
                  } else if (e.length < 16) {
                    return "Nomor KTP harus 16 digit";
                  } else {
                    return null;
                  }
                },
                autofocus: false,
                // onChanged: (e) {
                //   setState(() {
                //     _hasKtp = e.isNotEmpty;
                //     if (_hasKtp) {
                //       dealerCabang = e;
                //     }
                //   });
                // },
                // onSaved: (e) => nomerKTP = e,
                inputFormatters: [
                  WhitelistingTextInputFormatter.digitsOnly,
                  LengthLimitingTextInputFormatter(16),
                ],
              )),
          SizedBox(
            width: size.wp(2),
          ),
          // Expanded(
          //     flex: 4,
          //     child: RaisedButton(
          //       padding:
          //           EdgeInsets.only(top: size.hp(1.5), bottom: size.hp(1.5)),
          //       onPressed: () {
          //         // Navigator.of(context).push(MaterialPageRoute(
          //         //     builder: (context) =>
          //         //         GetDataProfileByOCR(dataKTP: setDataByOCR)));
          //       },
          //       shape: RoundedRectangleBorder(
          //           borderRadius: new BorderRadius.circular(8.0)),
          //       child: Text('Get profile with ocr',
          //           style: TextStyle(
          //               fontFamily: "NunitoSans", color: Colors.black)),
          //       color: kPrimaryColor,
          //     ))
        ],
      ),
      SizedBox(height: size.hp(2)),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            flex: 6,
            child: TextFormField(
              controller: _controllerRedeemVoucher,
              decoration: new InputDecoration(
                contentPadding:
                    EdgeInsets.only(top: 0.0, bottom: 0.0, right: 8, left: 8),
                labelText: 'Redeem voucher',
                labelStyle: TextStyle(fontFamily: "NunitoSans"),
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
              ),
              keyboardType: TextInputType.text,
              textCapitalization: TextCapitalization.characters,
              textInputAction: TextInputAction.done,
              focusNode: _focusNodeRedeemVoucher,
              enabled: _enableAutofilled,
              onFieldSubmitted: (term) {
                _focusNodeRedeemVoucher.unfocus();
              },
            ),
          ),
          SizedBox(width: size.wp(2)),
          _isProcessCheckVoucher
              ? _isUseVoucher == "true"
                  ? new Row(
                      children: <Widget>[
                        new IconButton(
                          icon: Icon(Icons.check, color: Colors.green),
                          onPressed: () {},
                        ),
                      ],
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    )
                  : _isUseVoucher == "false"
                      ? Expanded(
                          flex: 4,
                          child: RaisedButton(
                            padding: EdgeInsets.only(
                                top: size.hp(1.5), bottom: size.hp(1.5)),
                            onPressed: () {
                              //_checkValidVoucher();
                            },
                            shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(8.0)),
                            child: Text('Check voucher',
                                style: TextStyle(
                                    fontFamily: "NunitoSans",
                                    color: Colors.black)),
                            color: kPrimaryColor,
                          ))
                      : CircularProgressIndicator()
              : Expanded(
                  flex: 4,
                  child: RaisedButton(
                    padding: EdgeInsets.only(
                        top: size.hp(1.5), bottom: size.hp(1.5)),
                    onPressed: () {
                      //_checkValidVoucher();
                    },
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(8.0)),
                    child: Text('Check voucher',
                        style: TextStyle(
                            fontFamily: "NunitoSans", color: Colors.black)),
                    color: kPrimaryColor,
                  )),
          SizedBox(
            width: size.wp(2),
          ),
        ],
      ),
      SizedBox(height: size.hp(2)),
      TextFormField(
        autovalidate: _autoValidate3,
        controller: _controllerNamaLengkap,
        decoration: new InputDecoration(
          labelText: 'Nama Lengkap *',
          labelStyle: TextStyle(fontFamily: "NunitoSans"),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
        ),
        keyboardType: TextInputType.text,
        textCapitalization: TextCapitalization.sentences,
        textInputAction: TextInputAction.next,
        focusNode: _focusNodeNamaLengkap,
        onFieldSubmitted: (term) {
          _fieldFocusChange(
              context, _focusNodeNamaLengkap, _focusNodeTempatLahir);
        },
        enabled: _enableAutofilled,
        validator: (e) {
          if (e.isEmpty) {
            return "Tidak boleh kosong";
          } else {
            return null;
          }
        },
        onSaved: (e) => namaLengkap = e,
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp("[a-z A-Z]")),
          FilteringTextInputFormatter.deny(new RegExp(r'[-]'))
        ],
      ),
      SizedBox(height: size.hp(2)),
      TextFormField(
        autovalidate: _autoValidate3,
        controller: _controllerTempatLahir,
        decoration: new InputDecoration(
          labelText: 'Tempat Lahir *',
          labelStyle: TextStyle(fontFamily: "NunitoSans"),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
        ),
        keyboardType: TextInputType.text,
        textCapitalization: TextCapitalization.sentences,
        textInputAction: TextInputAction.done,
        focusNode: _focusNodeTempatLahir,
        onFieldSubmitted: (value) {
          _focusNodeTempatLahir.unfocus();
        },
        enabled: _enableAutofilled,
        validator: (e) {
          if (e.isEmpty) {
            return "Tidak boleh kosong";
          } else {
            return null;
          }
        },
        onSaved: (e) => tempatLahir = e,
      ),
      SizedBox(height: size.hp(2)),
      _enableAutofilled == false
          ? Row(children: <Widget>[
              Expanded(
                flex: 4,
                child: TextFormField(
                    autovalidate: _autoValidate3,
                    controller: _controllerThnlahir,
                    decoration: new InputDecoration(
                      labelText: 'Tahun Lahir *',
                      labelStyle: TextStyle(fontFamily: "NunitoSans"),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                    ),
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.sentences,
                    textInputAction: TextInputAction.done,
                    enabled: _enableAutofilled,
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    }),
              ),
              SizedBox(width: size.wp(2)),
              Expanded(
                flex: 4,
                child: TextFormField(
                    autovalidate: _autoValidate3,
                    controller: _controllerBlnlahir,
                    decoration: new InputDecoration(
                      labelText: 'Bulan *',
                      labelStyle: TextStyle(fontFamily: "NunitoSans"),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                    ),
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.sentences,
                    textInputAction: TextInputAction.done,
                    focusNode: _focusNodeTempatLahir,
                    enabled: _enableAutofilled,
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    }),
              ),
              SizedBox(width: size.wp(2)),
              Expanded(
                flex: 4,
                child: TextFormField(
                    autovalidate: _autoValidate3,
                    controller: _controllerTgllahir,
                    decoration: new InputDecoration(
                      labelText: 'Tanggal *',
                      labelStyle: TextStyle(fontFamily: "NunitoSans"),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                    ),
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.sentences,
                    textInputAction: TextInputAction.done,
                    focusNode: _focusNodeTempatLahir,
                    enabled: _enableAutofilled,
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    }),
              )
            ])
          : Row(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: DropdownButtonFormField<String>(
                    value: thnLahir,
                    onChanged: (String newVal) {
                      setState(() {
                        thnLahir = newVal;
                      });
                      if (blnLahir != null) {
                        _processDay();
                        _cekTahun();
                      }
                      FocusManager.instance.primaryFocus.unfocus();
                    },
                    validator: (e) {
                      if (e == null) {
                        return "Silahkan pilih tahun lahir";
                      } else {
                        return null;
                      }
                    },
                    autovalidate: _autoValidate3,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        labelText: "Tahun lahir *",
                        labelStyle: TextStyle(fontFamily: "NunitoSans"),
                        contentPadding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                        errorText: _validate ? "Umur < 21 tahun" : null),
                    items:
                        _listYear.map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(
                          value,
                          overflow: TextOverflow.ellipsis,
                        ),
                      );
                    }).toList(),
                  ),
                ),
                SizedBox(
                  width: 4,
                ),
                Expanded(
                  flex: 3,
                  child: DropdownButtonFormField<String>(
                    value: blnLahir,
                    onChanged: (String newVal) {
                      setState(() {
                        blnLahir = newVal;
                      });
                      if (thnLahir != null) {
                        _processDay();
                        _cekTahun();
                      }
                      FocusManager.instance.primaryFocus.unfocus();
                    },
                    validator: (e) {
                      if (e == null) {
                        return "Silahkan pilih bulan";
                      } else {
                        return null;
                      }
                    },
                    autovalidate: _autoValidate3,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        labelText: "Bulan *",
                        labelStyle: TextStyle(fontFamily: "NunitoSans"),
                        contentPadding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                        errorText: _validate ? "Umur < 21 tahun" : null),
                    items: _listMonth.map((map) {
                      return DropdownMenuItem<String>(
                        value: map['id'].toString(),
                        child: Text(
                          map['month'],
                          overflow: TextOverflow.ellipsis,
                        ),
                      );
                    }).toList(),
                  ),
                ),
                SizedBox(
                  width: 4,
                ),
                Expanded(
                  flex: 2,
                  child: DropdownButtonFormField<String>(
                    value: tglLahir,
                    onChanged: (String newVal) {
                      setState(() {
                        tglLahir = newVal;
                      });
                      if (blnLahir != null && thnLahir != null) {
                        _cekTahun();
                      }
                      FocusManager.instance.primaryFocus.unfocus();
                    },
                    validator: (e) {
                      if (e == null) {
                        return "Silahkan pilih tanggal";
                      } else {
                        return null;
                      }
                    },
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        labelText: "Tanggal *",
                        labelStyle: TextStyle(fontFamily: "NunitoSans"),
                        contentPadding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                        errorText: _validate ? "Umur < 21 tahun" : null),
                    items: _listDateBirthdate
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(
                          value,
                          overflow: TextOverflow.ellipsis,
                        ),
                      );
                    }).toList(),
                  ),
                ),
              ],
            ),
      SizedBox(height: size.hp(2)),
      DropdownButtonFormField<Gender>(
        value: _gender,
        onChanged: (gender) {
          setState(() {
            _gender = gender;
          });
          FocusManager.instance.primaryFocus.unfocus();
        },
        validator: (e) {
          if (e == null) {
            return "Silahkan pilih gender";
          } else {
            return null;
          }
        },
        autovalidate: _autoValidate3,
        decoration: InputDecoration(
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
            labelText: "Gender *",
            labelStyle: TextStyle(fontFamily: "NunitoSans"),
            contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 5)),
        items: listGender.map((Gender gender) {
          return new DropdownMenuItem<Gender>(
            value: gender,
            child: new Text(gender.genderName,
                style: new TextStyle(color: Colors.black)),
          );
        }).toList(),
      ),
      SizedBox(height: size.hp(2)),
      DropdownButtonFormField<String>(
        value: statusPernikahan,
        onChanged: (String newVal) {
          setState(() {
            statusPernikahan = newVal;
          });
          FocusManager.instance.primaryFocus.unfocus();
        },
        validator: (e) {
          if (e == null) {
            return "Silahkan pilih status pernikahan";
          } else {
            return null;
          }
        },
        autovalidate: _autoValidate3,
        decoration: InputDecoration(
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
            labelText: "Status Pernikahan *",
            labelStyle: TextStyle(fontFamily: "NunitoSans"),
            contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 5)),
        items: listStatusPernikahan.map((map) {
          return new DropdownMenuItem<String>(
            value: map['id'].toString(),
            child: new Text(map['statusKawin'],
                style: new TextStyle(color: Colors.black)),
          );
        }).toList(),
      ),
      SizedBox(height: size.hp(2)),
      TextFormField(
        autovalidate: _autoValidate3,
        controller: _controllerAlamatSurvey,
        decoration: new InputDecoration(
          labelText: 'Alamat Survey *',
          labelStyle: TextStyle(fontFamily: "NunitoSans"),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
        ),
        keyboardType: TextInputType.text,
        textCapitalization: TextCapitalization.sentences,
        textInputAction: TextInputAction.next,
        focusNode: _focusNodeAlamatSurvey,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _focusNodeAlamatSurvey, _focusNodeRT);
        },
        validator: (e) {
          if (e.isEmpty) {
            return "Tidak boleh kosong";
          } else {
            return null;
          }
        },
        onSaved: (e) => alamatSurvey = e,
        maxLines: 3,
      ),
      SizedBox(height: size.hp(2)),
      Row(
        children: <Widget>[
          Expanded(
              flex: 5,
              child: TextFormField(
                  autovalidate: _autoValidate3,
                  controller: _controllerRT,
                  decoration: new InputDecoration(
                    labelText: 'RT *',
                    labelStyle: TextStyle(fontFamily: "NunitoSans"),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                  ),
                  keyboardType: TextInputType.number,
                  textInputAction: TextInputAction.next,
                  focusNode: _focusNodeRT,
                  onFieldSubmitted: (term) {
                    _fieldFocusChange(context, _focusNodeRT, _focusNodeRW);
                  },
                  validator: (e) {
                    if (e.isEmpty) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  onSaved: (e) => alamatSurvey = e)),
          SizedBox(width: size.wp(1.5)),
          Expanded(
              flex: 5,
              child: TextFormField(
                  autovalidate: _autoValidate3,
                  controller: _controllerRW,
                  decoration: new InputDecoration(
                    labelText: 'RW *',
                    labelStyle: TextStyle(fontFamily: "NunitoSans"),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                  ),
                  keyboardType: TextInputType.number,
                  textInputAction: TextInputAction.done,
                  focusNode: _focusNodeRW,
                  onFieldSubmitted: (term) {
                    _focusNodeRW.unfocus();
                  },
                  validator: (e) {
                    if (e.isEmpty) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  onSaved: (e) => alamatSurvey = e))
        ],
      ),
      SizedBox(height: size.hp(2)),
      FocusScope(
        node: FocusScopeNode(),
        child: TextFormField(
          autovalidate: _autoValidate3,
          controller: _controllerKecamatan,
          decoration: new InputDecoration(
              labelText: 'Kecamatan *',
              labelStyle: TextStyle(fontFamily: "NunitoSans"),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
              suffixIcon: Icon(Icons.search, color: Colors.black)),
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) =>
                    SearchkecamatanPage(onSelected: setValueKecamatan)));
          },
          validator: (e) {
            if (e.isEmpty) {
              return "Tidak boleh kosong";
            } else {
              return null;
            }
          },
          onSaved: (e) => kecamatan = e,
//              onChanged: (e){
//                setState(() {
//                  _controllerKelurahan.text = e;
//                  _controllerKodePos.text = e;
//                });
//              },
        ),
      ),
      SizedBox(height: size.hp(2)),
      TextFormField(
        autovalidate: _autoValidate3,
        controller: _controllerKelurahan,
        decoration: new InputDecoration(
          labelText: 'Kelurahan *',
          labelStyle: TextStyle(fontFamily: "NunitoSans"),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
        ),
        enabled: false,
        validator: (e) {
          if (e.isEmpty) {
            return "Tidak boleh kosong";
          } else {
            return null;
          }
        },
        onSaved: (e) => kelurahan = e,
      ),
      SizedBox(height: size.hp(2)),
      TextFormField(
        autovalidate: _autoValidate3,
        controller: _controllerKodePos,
        decoration: new InputDecoration(
          labelText: 'Kode Pos *',
          labelStyle: TextStyle(fontFamily: "NunitoSans"),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
        ),
        enabled: false,
        validator: (e) {
          if (e.isEmpty) {
            return "Tidak boleh kosong";
          } else {
            return null;
          }
        },
        onSaved: (e) => kodePos = e,
      ),
      SizedBox(height: size.hp(2)),
      Container(
        child: Text("Alamat KTP (Sesuai Alamat Tinggal)",
            style: TextStyle(fontFamily: "NunitoSans")),
      ),
      Row(
        children: <Widget>[
          Radio(
            value: 0,
            groupValue: _radioValue,
            onChanged: _handleRadioValueChange,
            activeColor: const Color(0xff25AE88),
          ),
          Text("Ya", style: TextStyle(fontFamily: "NunitoSans")),
          Radio(
            value: 1,
            groupValue: _radioValue,
            onChanged: _handleRadioValueChange,
            activeColor: const Color(0xff25AE88),
          ),
          Text("Tidak", style: TextStyle(fontFamily: "NunitoSans")),
        ],
      ),
      SizedBox(height: size.hp(2)),
      _radioValue == 1
          ?
          //---alamat tidak sesuai ktp----//
          Column(
              children: <Widget>[
                TextFormField(
                  autovalidate: _autoValidate3,
                  controller: _controllerAlamatKTP,
                  decoration: new InputDecoration(
                    labelText: 'Alamat KTP ',
                    labelStyle: TextStyle(fontFamily: "NunitoSans"),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                  ),
                  keyboardType: TextInputType.text,
                  textCapitalization: TextCapitalization.sentences,
                  textInputAction: TextInputAction.next,
                  focusNode: _focusNodeAlamatKTP,
                  onFieldSubmitted: (term) {
                    _fieldFocusChange(
                        context, _focusNodeAlamatKTP, _focusNodeRtKtp);
                  },
                  onSaved: (e) => alamatKTP = e,
                  maxLines: 3,
                ),
                SizedBox(height: size.hp(2)),
                Row(
                  children: <Widget>[
                    Expanded(
                        flex: 5,
                        child: TextFormField(
                            autovalidate: _autoValidate3,
                            controller: _controllerRtKtp,
                            decoration: new InputDecoration(
                              labelText: 'RT ',
                              labelStyle: TextStyle(fontFamily: "NunitoSans"),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8)),
                            ),
                            keyboardType: TextInputType.number,
                            textInputAction: TextInputAction.next,
                            focusNode: _focusNodeRtKtp,
                            onFieldSubmitted: (term) {
                              _fieldFocusChange(
                                  context, _focusNodeRtKtp, _focusNodeRwKtp);
                            },
                            onSaved: (e) => alamatSurvey = e)),
                    SizedBox(width: size.wp(1.5)),
                    Expanded(
                        flex: 5,
                        child: TextFormField(
                            autovalidate: _autoValidate3,
                            controller: _controllerRwKtp,
                            decoration: new InputDecoration(
                              labelText: 'RW ',
                              labelStyle: TextStyle(fontFamily: "NunitoSans"),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8)),
                            ),
                            keyboardType: TextInputType.number,
                            textInputAction: TextInputAction.done,
                            focusNode: _focusNodeRwKtp,
                            onFieldSubmitted: (term) {
                              _focusNodeRwKtp.unfocus();
                            },
                            onSaved: (e) => alamatSurvey = e))
                  ],
                ),
                SizedBox(
                  height: size.hp(2),
                ),
                FocusScope(
                  node: FocusScopeNode(),
                  child: TextFormField(
                    autovalidate: _autoValidate3,
                    controller: _controllerKecamatanKTP,
                    decoration: new InputDecoration(
                        labelText: 'Kecamatan *',
                        labelStyle: TextStyle(fontFamily: "NunitoSans"),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        suffixIcon: Icon(Icons.search, color: Colors.black)),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => SearchkecamatanPage(
                              onSelected: setValueKecamatanKTP)));
//                    showSearch(context: context, delegate: SearchDelegateKecamatanKTP(_listResult,setValueKecamatanKTP));
                    },
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    onSaved: (e) => kecamatan = e,
                  ),
                ),
                SizedBox(height: size.hp(2)),
                TextFormField(
                  autovalidate: _autoValidate3,
                  controller: _controllerKelurahanKTP,
                  decoration: new InputDecoration(
                    labelText: 'Kelurahan ',
                    labelStyle: TextStyle(fontFamily: "NunitoSans"),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                  ),
                  keyboardType: TextInputType.text,
                  textCapitalization: TextCapitalization.sentences,
                  textInputAction: TextInputAction.next,
                  focusNode: _focusNodeKelurahanKTP,
                  onFieldSubmitted: (term) {
                    _fieldFocusChange(
                        context, _focusNodeKelurahanKTP, _focusNodeKodePosKTP);
                  },
                  onSaved: (e) => kelurahanKTP = e,
                ),
                SizedBox(height: size.hp(2)),
                TextFormField(
                  autovalidate: _autoValidate3,
                  controller: _controllerKodePosKTP,
                  decoration: new InputDecoration(
                    labelText: 'Kode Pos ',
                    labelStyle: TextStyle(fontFamily: "NunitoSans"),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                  ),
                  keyboardType: TextInputType.number,
                  textCapitalization: TextCapitalization.sentences,
                  textInputAction: TextInputAction.done,
                  focusNode: _focusNodeKodePosKTP,
                  onFieldSubmitted: (term) {
                    _focusNodeKodePosKTP.unfocus();
                  },
                  onSaved: (e) => kodePosKTP = e,
                ),
              ],
            )
          : SizedBox(height: 0.0, width: 0.0),
      SizedBox(height: size.hp(2)),
      Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: TextFormField(
              autovalidate: _autoValidate3,
              controller: _controllerKodeArea,
              decoration: new InputDecoration(
                labelText: 'Kode Area ',
                labelStyle: TextStyle(fontFamily: "NunitoSans"),
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
              ),
              keyboardType: TextInputType.number,
              textCapitalization: TextCapitalization.sentences,
              textInputAction: TextInputAction.next,
              focusNode: _focusNodeKodeArea,
              onFieldSubmitted: (term) {
                _fieldFocusChange(
                    context, _focusNodeKodeArea, _focusNodeTlpnRumah);
              },
              validator: (e) {
                if (e.isEmpty) {
                  return "Tidak boleh kosong";
                } else {
                  return null;
                }
              },
            ),
          ),
          SizedBox(width: 5),
          Expanded(
            flex: 6,
            child: TextFormField(
              autovalidate: _autoValidate3,
              controller: _controllerTlpnRumah,
              decoration: new InputDecoration(
                labelText: 'Telepon Rumah',
                labelStyle: TextStyle(fontFamily: "NunitoSans"),
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
              ),
              keyboardType: TextInputType.number,
              textCapitalization: TextCapitalization.sentences,
              textInputAction: TextInputAction.next,
              focusNode: _focusNodeTlpnRumah,
              onFieldSubmitted: (term) {
                _fieldFocusChange(
                    context, _focusNodeTlpnRumah, _focusNodeKodeNegara);
              },
//                  validator: (e) {
//                    if (e.isEmpty) {
//                      return "Tidak boleh kosong";
//                    }else{
//                      return null;
//                    }},
              onSaved: (e) => tlpnRumah = e,
            ),
          ),
        ],
      ),
      SizedBox(height: size.hp(2)),
      Row(
        children: <Widget>[
          Expanded(
            flex: 2,
            child: TextFormField(
              autovalidate: _autoValidate3,
              controller: _controllerKodeNegara,
              decoration: new InputDecoration(
                labelText: 'Kode Negara',
                labelStyle: TextStyle(fontFamily: "NunitoSans"),
                prefixText: "+",
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
              ),
              keyboardType: TextInputType.number,
              textCapitalization: TextCapitalization.sentences,
              textInputAction: TextInputAction.next,
              focusNode: _focusNodeKodeNegara,
              onFieldSubmitted: (term) {
                _fieldFocusChange(
                    context, _focusNodeKodeNegara, _focusNodeNoHp);
              },
//                  validator: (e) {
//                    if (e.isEmpty) {
//                      return "Tidak boleh kosong";
//                    }else{
//                      return null;
//                    }},
              onSaved: (e) => tlpnRumah = e,
//                  onChanged: (e){
//                    _cekZeroAreaCode(e);
//                  },
            ),
          ),
          SizedBox(
            width: 5,
          ),
          Expanded(
            flex: 6,
            child: TextFormField(
              autovalidate: _autoValidate3,
              controller: _controllerNoHp,
              decoration: new InputDecoration(
                labelText: 'No HP *',
                labelStyle: TextStyle(fontFamily: "NunitoSans"),
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
              ),
              keyboardType: TextInputType.number,
              textInputAction: TextInputAction.done,
              focusNode: _focusNodeNoHp,
              onFieldSubmitted: (term) {
                _focusNodeNoHp.unfocus();
              },
              validator: (e) {
                if (e.isEmpty) {
                  return "Tidak boleh kosong";
                } else {
                  return null;
                }
              },
              onSaved: (e) => noHp = e,
              inputFormatters: [
                WhitelistingTextInputFormatter.digitsOnly,
                LengthLimitingTextInputFormatter(12),
              ],
              onChanged: (e) {
                _cekZeroPhoneNumber(e);
              },
            ),
          ),
        ],
      ),
      SizedBox(height: size.hp(2)),
      DropdownButtonFormField<ResultPekerjaan>(
        value: _resultPekerjaan,
        onChanged: (newVal) {
          setState(() {
            _resultPekerjaan = newVal;
          });
          FocusManager.instance.primaryFocus.unfocus();
        },
        decoration: InputDecoration(
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
            labelText: "Pekerjaan",
            labelStyle: TextStyle(fontFamily: "NunitoSans"),
            contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 5)),
        items: _listResultPekerjaan.map((map) {
          return new DropdownMenuItem<ResultPekerjaan>(
            value: map,
            child: new Text(map.occupationDesc,
                style: new TextStyle(color: Colors.black)),
          );
        }).toList(),
      ),
      SizedBox(height: size.hp(2)),
      Card(
        shape: RoundedRectangleBorder(
          side: BorderSide(
            color: Colors.black,
            width: 0.2,
          ),
          borderRadius: BorderRadius.circular(8.0),
        ),
        elevation: 3.3,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height / 57,
                  left: MediaQuery.of(context).size.width / 37,
                  right: MediaQuery.of(context).size.width / 37),
              child: Text("Upload Foto KTP *",
                  style: TextStyle(fontFamily: "NunitoSansSemiBold")),
            ),
            Padding(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height / 57,
                  left: MediaQuery.of(context).size.width / 37,
                  right: MediaQuery.of(context).size.width / 37,
                  bottom: MediaQuery.of(context).size.height / 37),
              child: RaisedButton(
                onPressed: () {
                  //formMFotoChangeNotif.addFile(0);
                },
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(8.0)),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.camera_alt),
                    SizedBox(width: MediaQuery.of(context).size.width / 47),
                    Text("ADD")
                  ],
                ),
                color: kPrimaryColor,
              ),
            )
          ],
        ),
      )
    ]);
  }

  Column dataNasabahCompany() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
        Widget>[
      Row(
        children: [
          Expanded(
              flex: 6,
              child: TextFormField(
                autovalidate: _autoValidate3,
                controller: _controllerNomerKTP,
                decoration: new InputDecoration(
                  labelText: 'Nomor NPWP *',
                  labelStyle: TextStyle(fontFamily: "NunitoSans"),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8)),
                ),
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.number,
                focusNode: _focusNodeNoKTP,
                onFieldSubmitted: (term) {
                  _fieldFocusChange(
                      context, _focusNodeNoKTP, _focusNodeNamaLengkap);
                },
                enabled: _enableAutofilled,
                validator: (e) {
                  if (e.isEmpty) {
                    return "Tidak boleh kosong";
                  } else if (e.length < 16) {
                    return "Nomor KTP harus 16 digit";
                  } else {
                    return null;
                  }
                },
                autofocus: false,
                // onChanged: (e) {
                //   setState(() {
                //     _hasKtp = e.isNotEmpty;
                //     if (_hasKtp) {
                //       dealerCabang = e;
                //     }
                //   });
                // },
                // onSaved: (e) => nomerKTP = e,
                inputFormatters: [
                  WhitelistingTextInputFormatter.digitsOnly,
                  LengthLimitingTextInputFormatter(16),
                ],
              )),
          SizedBox(
            width: size.wp(2),
          ),
          // Expanded(
          //     flex: 4,
          //     child: RaisedButton(
          //       padding:
          //           EdgeInsets.only(top: size.hp(1.5), bottom: size.hp(1.5)),
          //       onPressed: () {
          //         // Navigator.of(context).push(MaterialPageRoute(
          //         //     builder: (context) =>
          //         //         GetDataProfileByOCR(dataKTP: setDataByOCR)));
          //       },
          //       shape: RoundedRectangleBorder(
          //           borderRadius: new BorderRadius.circular(8.0)),
          //       child: Text('Get profile with ocr',
          //           style: TextStyle(
          //               fontFamily: "NunitoSans", color: Colors.black)),
          //       color: kPrimaryColor,
          //     ))
        ],
      ),
      SizedBox(height: size.hp(2)),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            flex: 6,
            child: TextFormField(
              controller: _controllerRedeemVoucher,
              decoration: new InputDecoration(
                contentPadding:
                    EdgeInsets.only(top: 0.0, bottom: 0.0, right: 8, left: 8),
                labelText: 'Redeem voucher',
                labelStyle: TextStyle(fontFamily: "NunitoSans"),
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
              ),
              keyboardType: TextInputType.text,
              textCapitalization: TextCapitalization.characters,
              textInputAction: TextInputAction.done,
              focusNode: _focusNodeRedeemVoucher,
              enabled: _enableAutofilled,
              onFieldSubmitted: (term) {
                _focusNodeRedeemVoucher.unfocus();
              },
            ),
          ),
          SizedBox(width: size.wp(2)),
          _isProcessCheckVoucher
              ? _isUseVoucher == "true"
                  ? new Row(
                      children: <Widget>[
                        new IconButton(
                          icon: Icon(Icons.check, color: Colors.green),
                          onPressed: () {},
                        ),
                      ],
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    )
                  : _isUseVoucher == "false"
                      ? Expanded(
                          flex: 4,
                          child: RaisedButton(
                            padding: EdgeInsets.only(
                                top: size.hp(1.5), bottom: size.hp(1.5)),
                            onPressed: () {
                              //_checkValidVoucher();
                            },
                            shape: RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(8.0)),
                            child: Text('Check voucher',
                                style: TextStyle(
                                    fontFamily: "NunitoSans",
                                    color: Colors.black)),
                            color: kPrimaryColor,
                          ))
                      : CircularProgressIndicator()
              : Expanded(
                  flex: 4,
                  child: RaisedButton(
                    padding: EdgeInsets.only(
                        top: size.hp(1.5), bottom: size.hp(1.5)),
                    onPressed: () {
                      //_checkValidVoucher();
                    },
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(8.0)),
                    child: Text('Check voucher',
                        style: TextStyle(
                            fontFamily: "NunitoSans", color: Colors.black)),
                    color: kPrimaryColor,
                  )),
          SizedBox(
            width: size.wp(2),
          ),
        ],
      ),
      SizedBox(height: size.hp(2)),
      TextFormField(
        autovalidate: _autoValidate3,
        controller: _controllerNamaLengkap,
        decoration: new InputDecoration(
          labelText: 'Nama Perusahaan *',
          labelStyle: TextStyle(fontFamily: "NunitoSans"),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
        ),
        keyboardType: TextInputType.text,
        textCapitalization: TextCapitalization.sentences,
        textInputAction: TextInputAction.next,
        focusNode: _focusNodeNamaLengkap,
        onFieldSubmitted: (term) {
          _fieldFocusChange(
              context, _focusNodeNamaLengkap, _focusNodeTempatLahir);
        },
        enabled: _enableAutofilled,
        validator: (e) {
          if (e.isEmpty) {
            return "Tidak boleh kosong";
          } else {
            return null;
          }
        },
        onSaved: (e) => namaLengkap = e,
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp("[a-z A-Z]")),
          FilteringTextInputFormatter.deny(new RegExp(r'[-]'))
        ],
      ),
      SizedBox(height: size.hp(2)),
      TextFormField(
        autovalidate: _autoValidate3,
        controller: _controllerAlamatSurvey,
        decoration: new InputDecoration(
          labelText: 'Alamat Survey *',
          labelStyle: TextStyle(fontFamily: "NunitoSans"),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
        ),
        keyboardType: TextInputType.text,
        textCapitalization: TextCapitalization.sentences,
        textInputAction: TextInputAction.next,
        focusNode: _focusNodeAlamatSurvey,
        onFieldSubmitted: (term) {
          _fieldFocusChange(context, _focusNodeAlamatSurvey, _focusNodeRT);
        },
        validator: (e) {
          if (e.isEmpty) {
            return "Tidak boleh kosong";
          } else {
            return null;
          }
        },
        onSaved: (e) => alamatSurvey = e,
        maxLines: 3,
      ),
      SizedBox(height: size.hp(2)),
      Row(
        children: <Widget>[
          Expanded(
              flex: 5,
              child: TextFormField(
                  autovalidate: _autoValidate3,
                  controller: _controllerRT,
                  decoration: new InputDecoration(
                    labelText: 'RT *',
                    labelStyle: TextStyle(fontFamily: "NunitoSans"),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                  ),
                  keyboardType: TextInputType.number,
                  textInputAction: TextInputAction.next,
                  focusNode: _focusNodeRT,
                  onFieldSubmitted: (term) {
                    _fieldFocusChange(context, _focusNodeRT, _focusNodeRW);
                  },
                  validator: (e) {
                    if (e.isEmpty) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  onSaved: (e) => alamatSurvey = e)),
          SizedBox(width: size.wp(1.5)),
          Expanded(
              flex: 5,
              child: TextFormField(
                  autovalidate: _autoValidate3,
                  controller: _controllerRW,
                  decoration: new InputDecoration(
                    labelText: 'RW *',
                    labelStyle: TextStyle(fontFamily: "NunitoSans"),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                  ),
                  keyboardType: TextInputType.number,
                  textInputAction: TextInputAction.done,
                  focusNode: _focusNodeRW,
                  onFieldSubmitted: (term) {
                    _focusNodeRW.unfocus();
                  },
                  validator: (e) {
                    if (e.isEmpty) {
                      return "Tidak boleh kosong";
                    } else {
                      return null;
                    }
                  },
                  onSaved: (e) => alamatSurvey = e))
        ],
      ),
      SizedBox(height: size.hp(2)),
      FocusScope(
        node: FocusScopeNode(),
        child: TextFormField(
          autovalidate: _autoValidate3,
          controller: _controllerKecamatan,
          decoration: new InputDecoration(
              labelText: 'Kecamatan *',
              labelStyle: TextStyle(fontFamily: "NunitoSans"),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
              suffixIcon: Icon(Icons.search, color: Colors.black)),
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) =>
                    SearchkecamatanPage(onSelected: setValueKecamatan)));
          },
          validator: (e) {
            if (e.isEmpty) {
              return "Tidak boleh kosong";
            } else {
              return null;
            }
          },
          onSaved: (e) => kecamatan = e,
//              onChanged: (e){
//                setState(() {
//                  _controllerKelurahan.text = e;
//                  _controllerKodePos.text = e;
//                });
//              },
        ),
      ),
      SizedBox(height: size.hp(2)),
      TextFormField(
        autovalidate: _autoValidate3,
        controller: _controllerKelurahan,
        decoration: new InputDecoration(
          labelText: 'Kelurahan *',
          labelStyle: TextStyle(fontFamily: "NunitoSans"),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
        ),
        enabled: false,
        validator: (e) {
          if (e.isEmpty) {
            return "Tidak boleh kosong";
          } else {
            return null;
          }
        },
        onSaved: (e) => kelurahan = e,
      ),
      SizedBox(height: size.hp(2)),
      TextFormField(
        autovalidate: _autoValidate3,
        controller: _controllerKodePos,
        decoration: new InputDecoration(
          labelText: 'Kode Pos *',
          labelStyle: TextStyle(fontFamily: "NunitoSans"),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
        ),
        enabled: false,
        validator: (e) {
          if (e.isEmpty) {
            return "Tidak boleh kosong";
          } else {
            return null;
          }
        },
        onSaved: (e) => kodePos = e,
      ),
      SizedBox(height: size.hp(2)),
      Container(
        child: Text("Alamat NPWP (Sesuai Alamat Tinggal)",
            style: TextStyle(fontFamily: "NunitoSans")),
      ),
      Row(
        children: <Widget>[
          Radio(
            value: 0,
            groupValue: _radioValue,
            onChanged: _handleRadioValueChange,
            activeColor: const Color(0xff25AE88),
          ),
          Text("Ya", style: TextStyle(fontFamily: "NunitoSans")),
          Radio(
            value: 1,
            groupValue: _radioValue,
            onChanged: _handleRadioValueChange,
            activeColor: const Color(0xff25AE88),
          ),
          Text("Tidak", style: TextStyle(fontFamily: "NunitoSans")),
        ],
      ),
      SizedBox(height: size.hp(2)),
      _radioValue == 1
          ?
          //---alamat tidak sesuai ktp----//
          Column(
              children: <Widget>[
                TextFormField(
                  autovalidate: _autoValidate3,
                  controller: _controllerAlamatKTP,
                  decoration: new InputDecoration(
                    labelText: 'Alamat KTP ',
                    labelStyle: TextStyle(fontFamily: "NunitoSans"),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                  ),
                  keyboardType: TextInputType.text,
                  textCapitalization: TextCapitalization.sentences,
                  textInputAction: TextInputAction.next,
                  focusNode: _focusNodeAlamatKTP,
                  onFieldSubmitted: (term) {
                    _fieldFocusChange(
                        context, _focusNodeAlamatKTP, _focusNodeRtKtp);
                  },
                  onSaved: (e) => alamatKTP = e,
                  maxLines: 3,
                ),
                SizedBox(height: size.hp(2)),
                Row(
                  children: <Widget>[
                    Expanded(
                        flex: 5,
                        child: TextFormField(
                            autovalidate: _autoValidate3,
                            controller: _controllerRtKtp,
                            decoration: new InputDecoration(
                              labelText: 'RT ',
                              labelStyle: TextStyle(fontFamily: "NunitoSans"),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8)),
                            ),
                            keyboardType: TextInputType.number,
                            textInputAction: TextInputAction.next,
                            focusNode: _focusNodeRtKtp,
                            onFieldSubmitted: (term) {
                              _fieldFocusChange(
                                  context, _focusNodeRtKtp, _focusNodeRwKtp);
                            },
                            onSaved: (e) => alamatSurvey = e)),
                    SizedBox(width: size.wp(1.5)),
                    Expanded(
                        flex: 5,
                        child: TextFormField(
                            autovalidate: _autoValidate3,
                            controller: _controllerRwKtp,
                            decoration: new InputDecoration(
                              labelText: 'RW ',
                              labelStyle: TextStyle(fontFamily: "NunitoSans"),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8)),
                            ),
                            keyboardType: TextInputType.number,
                            textInputAction: TextInputAction.done,
                            focusNode: _focusNodeRwKtp,
                            onFieldSubmitted: (term) {
                              _focusNodeRwKtp.unfocus();
                            },
                            onSaved: (e) => alamatSurvey = e))
                  ],
                ),
                SizedBox(
                  height: size.hp(2),
                ),
                FocusScope(
                  node: FocusScopeNode(),
                  child: TextFormField(
                    autovalidate: _autoValidate3,
                    controller: _controllerKecamatanKTP,
                    decoration: new InputDecoration(
                        labelText: 'Kecamatan *',
                        labelStyle: TextStyle(fontFamily: "NunitoSans"),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)),
                        suffixIcon: Icon(Icons.search, color: Colors.black)),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => SearchkecamatanPage(
                              onSelected: setValueKecamatanKTP)));
//                    showSearch(context: context, delegate: SearchDelegateKecamatanKTP(_listResult,setValueKecamatanKTP));
                    },
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    onSaved: (e) => kecamatan = e,
                  ),
                ),
                SizedBox(height: size.hp(2)),
                TextFormField(
                  autovalidate: _autoValidate3,
                  controller: _controllerKelurahanKTP,
                  decoration: new InputDecoration(
                    labelText: 'Kelurahan ',
                    labelStyle: TextStyle(fontFamily: "NunitoSans"),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                  ),
                  keyboardType: TextInputType.text,
                  textCapitalization: TextCapitalization.sentences,
                  textInputAction: TextInputAction.next,
                  focusNode: _focusNodeKelurahanKTP,
                  onFieldSubmitted: (term) {
                    _fieldFocusChange(
                        context, _focusNodeKelurahanKTP, _focusNodeKodePosKTP);
                  },
                  onSaved: (e) => kelurahanKTP = e,
                ),
                SizedBox(height: size.hp(2)),
                TextFormField(
                  autovalidate: _autoValidate3,
                  controller: _controllerKodePosKTP,
                  decoration: new InputDecoration(
                    labelText: 'Kode Pos ',
                    labelStyle: TextStyle(fontFamily: "NunitoSans"),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                  ),
                  keyboardType: TextInputType.number,
                  textCapitalization: TextCapitalization.sentences,
                  textInputAction: TextInputAction.done,
                  focusNode: _focusNodeKodePosKTP,
                  onFieldSubmitted: (term) {
                    _focusNodeKodePosKTP.unfocus();
                  },
                  onSaved: (e) => kodePosKTP = e,
                ),
              ],
            )
          : SizedBox(height: 0.0, width: 0.0),
      SizedBox(height: size.hp(2)),
      Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: TextFormField(
              autovalidate: _autoValidate3,
              controller: _controllerKodeArea,
              decoration: new InputDecoration(
                labelText: 'Kode Area ',
                labelStyle: TextStyle(fontFamily: "NunitoSans"),
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
              ),
              keyboardType: TextInputType.number,
              textCapitalization: TextCapitalization.sentences,
              textInputAction: TextInputAction.next,
              focusNode: _focusNodeKodeArea,
              onFieldSubmitted: (term) {
                _fieldFocusChange(
                    context, _focusNodeKodeArea, _focusNodeTlpnRumah);
              },
              validator: (e) {
                if (e.isEmpty) {
                  return "Tidak boleh kosong";
                } else {
                  return null;
                }
              },
            ),
          ),
          SizedBox(width: 5),
          Expanded(
            flex: 6,
            child: TextFormField(
              autovalidate: _autoValidate3,
              controller: _controllerTlpnRumah,
              decoration: new InputDecoration(
                labelText: 'Telepon Rumah',
                labelStyle: TextStyle(fontFamily: "NunitoSans"),
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
              ),
              keyboardType: TextInputType.number,
              textCapitalization: TextCapitalization.sentences,
              textInputAction: TextInputAction.next,
              focusNode: _focusNodeTlpnRumah,
              onFieldSubmitted: (term) {
                _fieldFocusChange(
                    context, _focusNodeTlpnRumah, _focusNodeKodeNegara);
              },
//                  validator: (e) {
//                    if (e.isEmpty) {
//                      return "Tidak boleh kosong";
//                    }else{
//                      return null;
//                    }},
              onSaved: (e) => tlpnRumah = e,
            ),
          ),
        ],
      ),
      SizedBox(height: size.hp(2)),
      Row(
        children: <Widget>[
          Expanded(
            flex: 2,
            child: TextFormField(
              autovalidate: _autoValidate3,
              controller: _controllerKodeNegara,
              decoration: new InputDecoration(
                labelText: 'Kode Negara',
                labelStyle: TextStyle(fontFamily: "NunitoSans"),
                prefixText: "+",
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
              ),
              keyboardType: TextInputType.number,
              textCapitalization: TextCapitalization.sentences,
              textInputAction: TextInputAction.next,
              focusNode: _focusNodeKodeNegara,
              onFieldSubmitted: (term) {
                _fieldFocusChange(
                    context, _focusNodeKodeNegara, _focusNodeNoHp);
              },
//                  validator: (e) {
//                    if (e.isEmpty) {
//                      return "Tidak boleh kosong";
//                    }else{
//                      return null;
//                    }},
              onSaved: (e) => tlpnRumah = e,
//                  onChanged: (e){
//                    _cekZeroAreaCode(e);
//                  },
            ),
          ),
          SizedBox(
            width: 5,
          ),
          Expanded(
            flex: 6,
            child: TextFormField(
              autovalidate: _autoValidate3,
              controller: _controllerNoHp,
              decoration: new InputDecoration(
                labelText: 'No HP *',
                labelStyle: TextStyle(fontFamily: "NunitoSans"),
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
              ),
              keyboardType: TextInputType.number,
              textInputAction: TextInputAction.done,
              focusNode: _focusNodeNoHp,
              onFieldSubmitted: (term) {
                _focusNodeNoHp.unfocus();
              },
              validator: (e) {
                if (e.isEmpty) {
                  return "Tidak boleh kosong";
                } else {
                  return null;
                }
              },
              onSaved: (e) => noHp = e,
              inputFormatters: [
                WhitelistingTextInputFormatter.digitsOnly,
                LengthLimitingTextInputFormatter(12),
              ],
              onChanged: (e) {
                _cekZeroPhoneNumber(e);
              },
            ),
          ),
        ],
      ),
      SizedBox(height: size.hp(2)),
      Card(
        shape: RoundedRectangleBorder(
          side: BorderSide(
            color: Colors.black,
            width: 0.2,
          ),
          borderRadius: BorderRadius.circular(8.0),
        ),
        elevation: 3.3,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height / 57,
                  left: MediaQuery.of(context).size.width / 37,
                  right: MediaQuery.of(context).size.width / 37),
              child: Text("Upload Foto NPWP *",
                  style: TextStyle(fontFamily: "NunitoSansSemiBold")),
            ),
            Padding(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height / 57,
                  left: MediaQuery.of(context).size.width / 37,
                  right: MediaQuery.of(context).size.width / 37,
                  bottom: MediaQuery.of(context).size.height / 37),
              child: RaisedButton(
                onPressed: () {
                  //formMFotoChangeNotif.addFile(0);
                },
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(8.0)),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.camera_alt),
                    SizedBox(width: MediaQuery.of(context).size.width / 47),
                    Text("ADD")
                  ],
                ),
                color: kPrimaryColor,
              ),
            )
          ],
        ),
      )
    ]);
  }

  Widget _dataKendaraanStepWidget() {
    return Theme(
      data: ThemeData(primaryColor: Colors.black),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Text("Data Kendaraan",
                style:
                    TextStyle(fontFamily: "NunitoSansSemiBold", fontSize: 18)),
          ),
          SizedBox(
            height: size.hp(2),
          ),
          DropdownButtonFormField<JenisKendaraan>(
            value: _jenisKendaraan,
            onChanged: (jenisKendaraan) {
              setState(() {
                _jenisKendaraan = jenisKendaraan;
                _enableSearchModel = true;
              });
              _addTahunKendaraan();
              if (_controllerModelKendaraan.text.isNotEmpty) {
                _controllerModelKendaraan.clear();
                _controllerTipeKendaraan.clear();
                _controllerMerkKendaraan.clear();
              }
              FocusManager.instance.primaryFocus.unfocus();
            },
            decoration: InputDecoration(
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
              labelText: "Jenis kendaraan *",
              labelStyle: TextStyle(fontFamily: "NunitoSans"),
              contentPadding: EdgeInsets.symmetric(horizontal: 10),
            ),
            validator: (e) {
              if (e == null) {
                return "Silahkan pilih jenis kendaraan";
              } else {
                return null;
              }
            },
            autovalidate: _autoValidate,
            items: listJenisKendaraan.map((JenisKendaraan jenisKendaraan) {
              return new DropdownMenuItem<JenisKendaraan>(
                value: jenisKendaraan,
                child: new Text(jenisKendaraan.jenisKendaraan,
                    style: new TextStyle(color: Colors.black)),
              );
            }).toList(),
          ),
          SizedBox(height: size.hp(2)),
          FocusScope(
            node: FocusScopeNode(),
            child: TextFormField(
              autovalidate: _autoValidate,
              controller: _controllerModelKendaraan,
              decoration: new InputDecoration(
                  labelText: 'Merk *',
                  labelStyle: TextStyle(fontFamily: "NunitoSans"),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8)),
                  suffixIcon: Icon(Icons.search, color: Colors.black)),
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => SearchModelKendaraan(
                        onSelected: _setValueModel,
                        idStatusKendaraan: _jenisKendaraan.id)));
              },
              enabled: _enableSearchModel,
              validator: (e) {
                if (e.isEmpty) {
                  return "Tidak boleh kosong";
                } else {
                  return null;
                }
              },
              onSaved: (e) => modelKendaraan = e,
            ),
          ),
          SizedBox(height: size.hp(2)),
          TextFormField(
            autovalidate: _autoValidate,
            controller: _controllerTipeKendaraan,
            decoration: new InputDecoration(
              labelText: 'Tipe',
              labelStyle: TextStyle(fontFamily: "NunitoSans"),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
            ),
            enabled: false,
            validator: (e) {
              if (e.isEmpty) {
                return "Tidak boleh kosong";
              } else {
                return null;
              }
            },
            onSaved: (e) => tipeKendaraan = e,
          ),
          SizedBox(height: size.hp(2)),
          TextFormField(
            autovalidate: _autoValidate,
            controller: _controllerMerkKendaraan,
            decoration: new InputDecoration(
              labelText: 'Model',
              labelStyle: TextStyle(fontFamily: "NunitoSans"),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
            ),
            enabled: false,
            validator: (e) {
              if (e.isEmpty) {
                return "Tidak boleh kosong";
              } else {
                return null;
              }
            },
            onSaved: (e) => merkKendaraan = e,
          ),
          SizedBox(height: size.hp(2)),
          SizedBox(
            height: size.hp(2),
          ),
          _jenisKendaraan == null || _jenisKendaraan.id == "005"
              ? TextFormField(
                  autovalidate: _autoValidate,
                  controller: _controllerNamaPadaBPKB, //belum diganti
                  decoration: new InputDecoration(
                    labelText: 'Model Detail',
                    labelStyle: TextStyle(fontFamily: "NunitoSans"),
                    hintText: "Isi Model Detail",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                  ),
                  keyboardType: TextInputType.text,
                  textCapitalization: TextCapitalization.sentences,
                  textInputAction: TextInputAction.done,
                  focusNode: _focusNodeNamaPadaBPKB,
                  autofocus: false,
                  onSaved: (e) => namaPadaBPKB = e,
                )
              : SizedBox(height: 0.0, width: 0.0),
          _jenisKendaraan == null ||
                  _jenisKendaraan.id == "002" ||
                  _jenisKendaraan.id == "004"
              ? DropdownButtonFormField<String>(
                  value: _tahunKendaraanDipilih,
                  onChanged: (String newVal) {
                    setState(() {
                      _tahunKendaraanDipilih = newVal;
                    });
                    FocusManager.instance.primaryFocus.unfocus();
                  },
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                    labelText: "Tahun Kendaraan ",
                    labelStyle: TextStyle(fontFamily: "NunitoSans"),
                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                  ),
                  autovalidate: _autoValidate,
                  items: _listTahunKendaraan
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(
                        value,
                        overflow: TextOverflow.ellipsis,
                      ),
                    );
                  }).toList(),
                )
              : SizedBox(height: 0.0, width: 0.0),
          SizedBox(
            height: size.hp(2),
          ),
          _jenisKendaraan == null ||
                  _jenisKendaraan.id == "002" ||
                  _jenisKendaraan.id == "004"
              ? TextFormField(
                  autovalidate: _autoValidate,
                  controller: _controllerNamaPadaBPKB,
                  decoration: new InputDecoration(
                    labelText: 'Nama pada BPKB',
                    labelStyle: TextStyle(fontFamily: "NunitoSans"),
                    hintText: "Isi Nama",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                  ),
                  keyboardType: TextInputType.text,
                  textCapitalization: TextCapitalization.sentences,
                  textInputAction: TextInputAction.done,
                  focusNode: _focusNodeNamaPadaBPKB,
                  autofocus: false,
//            onFieldSubmitted: (value){
//              _focusNodeNamaPadaBPKB.unfocus();
//            },

                  onSaved: (e) => namaPadaBPKB = e,
                )
              : SizedBox(height: 0.0, width: 0.0),
          SizedBox(height: size.hp(2)),
          (_jenisKendaraan == null ||
                  _jenisKendaraan.id == '002' ||
                  _jenisKendaraan.id == '004')
              ? Card(
                  shape: RoundedRectangleBorder(
                    side: BorderSide(
                      color: Colors.black,
                      width: 0.2,
                    ),
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  elevation: 3.3,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height / 57,
                            left: MediaQuery.of(context).size.width / 37,
                            right: MediaQuery.of(context).size.width / 37),
                        child: Text("Upload Foto STNK *",
                            style: TextStyle(fontFamily: "NunitoSansSemiBold")),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height / 57,
                            left: MediaQuery.of(context).size.width / 37,
                            right: MediaQuery.of(context).size.width / 37,
                            bottom: MediaQuery.of(context).size.height / 37),
                        child: RaisedButton(
                          onPressed: () {
                            //formMFotoChangeNotif.addFile(0);
                          },
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(8.0)),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(Icons.camera_alt),
                              SizedBox(
                                  width:
                                      MediaQuery.of(context).size.width / 47),
                              Text("ADD")
                            ],
                          ),
                          color: kPrimaryColor,
                        ),
                      )
                    ],
                  ),
                )
              : SizedBox(height: 0.0, width: 0.0),
          SizedBox(height: size.hp(2)),
          (_jenisKendaraan == null || _jenisKendaraan.id == '002')
              ? Card(
                  shape: RoundedRectangleBorder(
                    side: BorderSide(
                      color: Colors.black,
                      width: 0.2,
                    ),
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  elevation: 3.3,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height / 57,
                            left: MediaQuery.of(context).size.width / 37,
                            right: MediaQuery.of(context).size.width / 37),
                        child: Text("Upload Foto Kendaraan 4 Sisi",
                            style: TextStyle(fontFamily: "NunitoSansSemiBold")),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height / 57,
                            left: MediaQuery.of(context).size.width / 37,
                            right: MediaQuery.of(context).size.width / 37,
                            bottom: MediaQuery.of(context).size.height / 37),
                        child: RaisedButton(
                          onPressed: () {
                            //formMFotoChangeNotif.addFile(0);
                          },
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(8.0)),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(Icons.camera_alt),
                              SizedBox(
                                  width:
                                      MediaQuery.of(context).size.width / 47),
                              Text("ADD")
                            ],
                          ),
                          color: kPrimaryColor,
                        ),
                      )
                    ],
                  ),
                )
              : SizedBox(height: 0.0, width: 0.0),
        ],
      ),
    );
  }

  Widget _dataPengajuanKredit() {
    return Theme(
      data: ThemeData(primaryColor: Colors.black),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Text("Data Pengajuan Kredit",
                style:
                    TextStyle(fontFamily: "NunitoSansSemiBold", fontSize: 18)),
          ),
          SizedBox(height: size.hp(3)),
          DropdownButtonFormField<JenisPembiayaan>(
            value: _jenisPembiayaan,
            onChanged: (newValJenisPembiayaan) {
              setState(() {
                _jenisPembiayaan = newValJenisPembiayaan;
              });
              FocusManager.instance.primaryFocus.unfocus();
            },
            validator: (e) {
              if (e == null) {
                return "Silahkan pilih jenis pembiayaan";
              } else {
                return null;
              }
            },
            autovalidate: _autoValidate2,
            decoration: InputDecoration(
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
              labelText: "Jenis Pembiayaan",
              labelStyle: TextStyle(fontFamily: "NunitoSans"),
              contentPadding: EdgeInsets.symmetric(horizontal: 10),
            ),
            items: listJenisPembiayaan.map((JenisPembiayaan jenisPembiayaan) {
              return new DropdownMenuItem<JenisPembiayaan>(
                value: jenisPembiayaan,
                child: new Text(jenisPembiayaan._jenisPembiayaanName,
                    style: new TextStyle(color: Colors.black)),
              );
            }).toList(),
          ),
          SizedBox(height: size.hp(2)),
          (_jenisKendaraan == null || _jenisKendaraan.id == '002')
              ? DropdownButtonFormField<ProdukPembiayaan>(
                  value: _produkPembiayaan,
                  onChanged: (newValProdukPembiayaan) {
                    setState(() {
                      _produkPembiayaan = newValProdukPembiayaan;
                    });
                    FocusManager.instance.primaryFocus.unfocus();
                  },
                  validator: (e) {
                    if (e == null) {
                      return "Silahkan pilih produk pembiayaan";
                    } else {
                      return null;
                    }
                  },
                  autovalidate: _autoValidate2,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                    labelText: "Produk Pembiayaan",
                    labelStyle: TextStyle(fontFamily: "NunitoSans"),
                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                  ),
                  items: listProdukPembiayaan
                      .map((ProdukPembiayaan produkPembiayaan) {
                    return new DropdownMenuItem<ProdukPembiayaan>(
                      value: produkPembiayaan,
                      child: new Text(produkPembiayaan._produkPembiayaanName,
                          style: new TextStyle(color: Colors.black)),
                    );
                  }).toList(),
                )
              : SizedBox(height: 0.0, width: 0.0),
          SizedBox(height: size.hp(2)),
          (_jenisKendaraan == null ||
                  _jenisKendaraan.id == '003' ||
                  _jenisKendaraan.id == '004')
              ? DropdownButtonFormField<JenisAsuransi>(
                  value: _jenisAsuransi,
                  onChanged: (newValJenisAsuransi) {
                    setState(() {
                      _jenisAsuransi = newValJenisAsuransi;
                    });
                    FocusManager.instance.primaryFocus.unfocus();
                  },
                  validator: (e) {
                    if (e == null) {
                      return "Silahkan pilih jenis asuransi";
                    } else {
                      return null;
                    }
                  },
                  autovalidate: _autoValidate2,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                    labelText: "Jenis Asuransi",
                    labelStyle: TextStyle(fontFamily: "NunitoSans"),
                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                  ),
                  items: listJenisAsuransi.map((JenisAsuransi jenisAsuransi) {
                    return new DropdownMenuItem<JenisAsuransi>(
                      value: jenisAsuransi,
                      child: new Text(jenisAsuransi._jenisAsuransiName,
                          style: new TextStyle(color: Colors.black)),
                    );
                  }).toList(),
                )
              : SizedBox(height: 0.0, width: 0.0),
          SizedBox(height: size.hp(2)),
          TextFormField(
            autovalidate: _autoValidate2,
            controller: _controllerOTR,
            decoration: new InputDecoration(
              labelText: (_jenisKendaraan == null ||
                      _jenisKendaraan.id == '001' ||
                      _jenisKendaraan.id == '002' ||
                      _jenisKendaraan.id == '003' ||
                      _jenisKendaraan.id == '004')
                  ? 'OTR (Rp) '
                  : 'Harga Barang (Rp) ',
              labelStyle: TextStyle(fontFamily: "NunitoSans"),
              hintText: "19999999",
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
            ),
            inputFormatters: [
              WhitelistingTextInputFormatter.digitsOnly,
              LengthLimitingTextInputFormatter(10),
              CurrencyFormat()
            ],
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.number,
            focusNode: _focusNodeOTR,
            onFieldSubmitted: (term) {
              _focusNodeOTR.unfocus();
            },
            onSaved: (e) => otr = e,
          ),
          SizedBox(height: size.hp(2)),
          DropdownButtonFormField<String>(
            value: _tenor,
            onChanged: (String newVal) {
              setState(() {
                _tenor = newVal;
              });
              FocusManager.instance.primaryFocus.unfocus();
            },
            validator: (e) {
              if (e == null) {
                return "Silahkan pilih tenor";
              } else {
                return null;
              }
            },
            autovalidate: _autoValidate2,
            decoration: InputDecoration(
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
              labelText: "Tenor ",
              labelStyle: TextStyle(fontFamily: "NunitoSans"),
              contentPadding: EdgeInsets.symmetric(horizontal: 10),
            ),
            items:
                _listPilihanTenor.map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(
                  value,
                  overflow: TextOverflow.ellipsis,
                ),
              );
            }).toList(),
          ),
          SizedBox(height: size.hp(2)),
          TextFormField(
            autovalidate: _autoValidate2,
            controller: _controllerDP,
            decoration: new InputDecoration(
              labelText: 'DP ',
              hintText: '9999999',
              labelStyle: TextStyle(fontFamily: "NunitoSans"),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
            ),
            inputFormatters: [
              WhitelistingTextInputFormatter.digitsOnly,
              CurrencyFormat()
            ],
            textInputAction: TextInputAction.next,
            keyboardType: TextInputType.number,
            focusNode: _focusNodeDp,
            onFieldSubmitted: (term) {
              _fieldFocusChange(context, _focusNodeDp, _focusNodeAngsuran);
            },
            validator: (e) {
              if (_dpIsBiggerThenOTR()) {
                return "DP tidak bisa lebih besar atau sama dengan OTR";
              } else {
                return null;
              }
            },
            onSaved: (e) => otr = e,
          ),
          SizedBox(height: size.hp(2)),
          TextFormField(
            autovalidate: _autoValidate2,
            controller: _controllerAngsuran,
            decoration: new InputDecoration(
              labelText: 'Angsuran ',
              hintText: '999999',
              labelStyle: TextStyle(fontFamily: "NunitoSans"),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
            ),
            inputFormatters: [
              WhitelistingTextInputFormatter.digitsOnly,
              CurrencyFormat()
            ],
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.done,
            focusNode: _focusNodeAngsuran,
            onFieldSubmitted: (value) {
              _focusNodeAngsuran.unfocus();
            },
            onSaved: (e) => _angsuran = e,
          ),
        ],
      ),
    );
  }

  Widget _dataLain() {
    return Theme(
        data: ThemeData(primaryColor: Colors.black),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Text("Data Lain",
                  style: TextStyle(
                      fontFamily: "NunitoSansSemiBold", fontSize: 18)),
            ),
            SizedBox(height: size.hp(3)),
            DropdownButtonFormField<PemilihanCaraSurvey>(
              value: _pemilihanCaraSurvey,
              onChanged: (newVal) {
                // setState(() {
                //   _pemilihanCaraSurvey = newVal;
                // });
                // print(newVal._id);
                // _getGenerateDate();
                FocusManager.instance.primaryFocus.unfocus();
              },
              validator: (e) {
                if (e == null) {
                  return "Silahkan pilih cara survey";
                } else {
                  return null;
                }
              },
              autovalidate: _autoValidate4,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8)),
                  labelText: "Pemilihan Cara Survey",
                  labelStyle: TextStyle(fontFamily: "NunitoSans"),
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 10, vertical: 5)),
              items: _listPemilihanCaraSurvey
                  .map((PemilihanCaraSurvey pemilihanCaraSurvey) {
                return new DropdownMenuItem<PemilihanCaraSurvey>(
                  value: pemilihanCaraSurvey,
                  child: new Text(pemilihanCaraSurvey._pemilihanCaraSurvey,
                      style: new TextStyle(color: Colors.black)),
                );
              }).toList(),
            ),
            SizedBox(height: size.hp(2)),
            _pemilihanCaraSurvey._id == 2
                ? Column(
                    children: <Widget>[
                      DropdownButtonFormField<Branch>(
                        value: _pilihanCabang,
                        onChanged: (newVal) {
                          setState(() {
                            _pilihanCabang = newVal;
                          });
                          _getDateList(newVal._id);
                          FocusManager.instance.primaryFocus.unfocus();
                        },
                        validator: (e) {
                          if (e == null) {
                            return "Silahkan pilih dealer cabang";
                          } else {
                            return null;
                          }
                        },
                        autovalidate: _autoValidate4,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(8)),
                            labelText: "Pilih cabang",
                            labelStyle: TextStyle(fontFamily: "NunitoSans"),
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 10, vertical: 5)),
                        items: _listBranch.map((Branch value) {
                          return DropdownMenuItem<Branch>(
                            value: value,
                            child: Text(
                              value._branchName,
                              overflow: TextOverflow.ellipsis,
                            ),
                          );
                        }).toList(),
                      ),
                      SizedBox(height: size.hp(2)),
                    ],
                  )
                :

                //add DAK
                _pemilihanCaraSurvey._id == 5
                    ? Column(
                        children: <Widget>[
                          DropdownButtonFormField<PemilihanMktDedicated>(
                            value: _pilihanMktDedicated,
                            onChanged: (newVal) {
                              setState(() {
                                _pilihanMktDedicated = newVal;
                              });
                              _getDateByNbmByNik(newVal._nik);
                              FocusManager.instance.primaryFocus.unfocus();
                            },
                            validator: (e) {
                              if (e == null) {
                                return "Silahkan pilih Marketingnya";
                              } else {
                                return null;
                              }
                            },
                            autovalidate: _autoValidate4,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                labelText: "Pilih Marketing *",
                                labelStyle: TextStyle(fontFamily: "NunitoSans"),
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 5)),
                            items: _listMkt.map((PemilihanMktDedicated value) {
                              return DropdownMenuItem<PemilihanMktDedicated>(
                                value: value,
                                child: Text(
                                  "${value._nik} - ${value._employeeName}",
                                  overflow: TextOverflow.ellipsis,
                                ),
                              );
                            }).toList(),
                          ),
                          SizedBox(height: size.hp(2)),
                        ],
                      )
                    : _pemilihanCaraSurvey._id == 4
                        ? Column(
                            children: <Widget>[
                              DropdownButtonFormField<Branch>(
                                value: _pilihanCabang,
                                onChanged: (newVal) {
                                  setState(() {
                                    _pilihanCabang = newVal;
                                  });
                                  _getDateList(newVal._id);
                                  FocusManager.instance.primaryFocus.unfocus();
                                },
                                validator: (e) {
                                  if (e == null) {
                                    return "Silahkan pilih dealer cabang";
                                  } else {
                                    return null;
                                  }
                                },
                                autovalidate: _autoValidate4,
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(8)),
                                    labelText: "Pilih cabang",
                                    labelStyle:
                                        TextStyle(fontFamily: "NunitoSans"),
                                    contentPadding: EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 5)),
                                items: _listBranch.map((Branch value) {
                                  return DropdownMenuItem<Branch>(
                                    value: value,
                                    child: Text(
                                      value._branchName.trim(),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  );
                                }).toList(),
                              ),
                              SizedBox(height: size.hp(2)),
                            ],
                          )
                        : SizedBox(height: 0.0, width: 0.0),
            DropdownButtonFormField<String>(
              value: _surveyDateSelected,
              onChanged: (String newVal) {
                setState(() {
                  _surveyDateSelected = newVal;
                });
                if (_pemilihanCaraSurvey._id == 2 ||
                    _pemilihanCaraSurvey._id == 4) {
                  _getTimeByDateAutoBranchOrByAsIs(newVal);
                } else if (_pemilihanCaraSurvey._id == 1) {
                  _getTimeByDateAutoAssign(newVal);
                } else {
                  _getTimeByNbm(_pilihanMktDedicated._nik, newVal);
                }
                FocusManager.instance.primaryFocus.unfocus();
              },
              validator: (e) {
                if (e == null) {
                  return "Silahkan pilih tanggal survey";
                } else {
                  return null;
                }
              },
              autovalidate: _autoValidate4,
              decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                labelText: "Pilih tanggal survey *",
                labelStyle: TextStyle(fontFamily: "NunitoSans"),
                contentPadding: EdgeInsets.symmetric(horizontal: 10),
              ),
              items: _dateListFix.map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(
                    value,
                    overflow: TextOverflow.ellipsis,
                  ),
                );
              }).toList(),
            ),
            SizedBox(height: size.hp(2)),
            Row(
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: DropdownButtonFormField<String>(
                    value: _surveyJamSelected,
                    onChanged: (String newVal) {
                      setState(() {
                        _surveyJamSelected = newVal;
                      });
                      FocusManager.instance.primaryFocus.unfocus();
                    },
                    validator: (e) {
                      if (e == null) {
                        return "Silahkan pilih jam survey";
                      } else {
                        return null;
                      }
                    },
                    autovalidate: _autoValidate4,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                      labelText: "Pilih jam survey *",
                      labelStyle: TextStyle(fontFamily: "NunitoSans"),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                    ),
                    items: _listHour.map((String _myValue) {
                      return DropdownMenuItem(
                        value: _myValue,
                        child: Text(
                          _myValue,
                          overflow: TextOverflow.ellipsis,
                        ),
                      );
                    }).toList(),
                  ),
                ),
                SizedBox(
                  width: size.wp(3),
                ),
                Expanded(
                  flex: 3,
                  child: DropdownButtonFormField<String>(
                    value: _surveyMenitSelected,
                    onChanged: (String newVal) {
                      setState(() {
                        _surveyMenitSelected = newVal;
                      });
                      FocusManager.instance.primaryFocus.unfocus();
                    },
                    validator: (e) {
                      if (e == null) {
                        return "Silahkan pilih menit survey";
                      } else {
                        return null;
                      }
                    },
                    autovalidate: _autoValidate4,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                      labelText: "Pilih menit survey *",
                      labelStyle: TextStyle(fontFamily: "NunitoSans"),
                      contentPadding: EdgeInsets.symmetric(horizontal: 10),
                    ),
                    items: _listMinute
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(
                          value,
                          overflow: TextOverflow.ellipsis,
                        ),
                      );
                    }).toList(),
                  ),
                ),
              ],
            ),
            SizedBox(height: size.hp(2)),
            FocusScope(
              node: FocusScopeNode(),
              child: TextFormField(
                controller: _controllerLocation,
                decoration: new InputDecoration(
                  labelText: 'Location',
                  labelStyle: TextStyle(fontFamily: "NunitoSans"),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8)),
                ),
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => MapPage(
                            setAddress: setAddressByMap,
                          )));
                },
                maxLines: 3,
              ),
            ),
            SizedBox(height: size.hp(2)),
            _radioCustTypeValue == 0
                ? TextFormField(
                    autovalidate: _autoValidate4,
                    controller: _controllerNamaGadisIbuKandung,
                    decoration: new InputDecoration(
                      labelText: 'Nama Lengkap Gadis Ibu Kandung *',
                      labelStyle: TextStyle(fontFamily: "NunitoSans"),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8)),
                    ),
                    onChanged: (term) {
                      _validatefinish = true;
                    },
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.sentences,
                    textInputAction: TextInputAction.next,
                    focusNode: _focusNodeNamaIbuKandung,
                    onFieldSubmitted: (term) {
                      _fieldFocusChange(context, _focusNodeNamaIbuKandung,
                          _focusNodeCatatanDealer);
                    },
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp("[a-z A-Z]")),
                      FilteringTextInputFormatter.deny(new RegExp(r'[-]'))
                    ],
                    validator: (e) {
                      if (e.isEmpty) {
                        return "Tidak boleh kosong";
                      } else {
                        return null;
                      }
                    },
                    onSaved: (e) => namaGadisIbuKandung = e,
                  )
                : SizedBox(height: 0.0, width: 0.0),
            SizedBox(height: size.hp(2)),
            TextFormField(
              autovalidate: _autoValidate4,
              controller: _controllerCatatanDealer,
              decoration: new InputDecoration(
                labelText: 'Catatan Dealer',
                labelStyle: TextStyle(fontFamily: "NunitoSans"),
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
              ),
              keyboardType: TextInputType.text,
              textCapitalization: TextCapitalization.sentences,
              textInputAction: TextInputAction.done,
              focusNode: _focusNodeCatatanDealer,
              onFieldSubmitted: (term) {
                _focusNodeCatatanDealer.unfocus();
              },
//              validator: (e) {
//                if (e.isEmpty) {
//                  return "Tidak boleh kosong";
//                }else{
//                  return null;
//                }},
              onSaved: (e) => catatanDealer = e,
              maxLines: 3,
            ),
          ],
        ));
  }

  DateTime dateNowIssue = new DateTime.now();
  String chooseIssueDate;

  var hariIni = DateTime.now().day;
  var bulanIni = DateTime.now().month;

  int cekUmur = 0;

  bool _cekTahun() {
    var dDay = new DateTime.now();
    var dateSelected =
        DateTime(int.parse(thnLahir), int.parse(blnLahir), int.parse(tglLahir));
    Duration difference = dDay.difference(dateSelected);
    print(difference.inDays);
    if (difference.inDays < 7670) {
      setState(() {
        _validate = true;
      });
      _showSnackBar("Data yang anda masukkan salah");
      return true;
    } else {
      setState(() {
        _validate = false;
      });
      return false;
    }
  }

  List<String> _listYear = [];
  List _listMonth = [
    {"id": 1, "month": "January"},
    {"id": 2, "month": "February"},
    {"id": 3, "month": "March"},
    {"id": 4, "month": "April"},
    {"id": 5, "month": "May"},
    {"id": 6, "month": "June"},
    {"id": 7, "month": "July"},
    {"id": 8, "month": "August"},
    {"id": 9, "month": "September"},
    {"id": 10, "month": "October"},
    {"id": 11, "month": "November"},
    {"id": 12, "month": "December"}
  ];
  List<String> _listDateBirthdate = [];

  processDate() {
    var now = DateTime.now();
    var limitYearDown = now.year - 59;
    var limitYearUp = now.year - 21;

    for (var i = limitYearDown; i <= limitYearUp; i++) {
      _listYear.add(i.toString());
    }

    for (var i = 1; i <= 31; i++) {
      _listDateBirthdate.add(i.toString());
    }
  }

  check() {
    final form = formKeys[_currentStep].currentState;
    if (_currentStep == 0) {
      if (form.validate()) {
        if (_cekTahun()) {
          _validate = true;
        } else {
          form.save();
          _onStepContinue();
        }
      } else {
        setState(() {
          _autoValidate3 = true;
        });
      }
    } else if (_currentStep == 1) {
      if (form.validate()) {
        form.save();
        _onStepContinue();
      } else {
        setState(() {
          _autoValidate = true;
        });
      }
    } else if (_currentStep == 2) {
      if (form.validate()) {
        form.save();
        if (_dpIsBiggerThenOTR()) {
          setState(() {
            _autoValidate2 = true;
          });
        } else {
          _onStepContinue();
        }
      } else {
        setState(() {
          _autoValidate2 = true;
        });
      }
    } else {
      if (form.validate()) {
        form.save();
        _showDialog();
      } else {
        setState(() {
          _autoValidate4 = true;
        });
      }
    }
  }

  _processDay() {
    int thnLahirSelected = int.parse(thnLahir);
    int blnLahirSelected;

    if (blnLahir == "1") {
      blnLahirSelected = 2;
    } else if (blnLahir == "2") {
      blnLahirSelected = 3;
    } else if (blnLahir == "3") {
      blnLahirSelected = 4;
    } else if (blnLahir == "4") {
      blnLahirSelected = 5;
    } else if (blnLahir == "5") {
      blnLahirSelected = 6;
    } else if (blnLahir == "6") {
      blnLahirSelected = 7;
    } else if (blnLahir == "7") {
      blnLahirSelected = 8;
    } else if (blnLahir == "8") {
      blnLahirSelected = 9;
    } else if (blnLahir == "9") {
      blnLahirSelected = 10;
    } else if (blnLahir == "10") {
      blnLahirSelected = 11;
    } else if (blnLahir == "11") {
      blnLahirSelected = 12;
    } else if (blnLahir == "12") {
      blnLahirSelected = 1;
    }
    print("tahun lahir $thnLahirSelected");
    print("bulan lahir $blnLahirSelected");
    var date = new DateTime(thnLahirSelected, blnLahirSelected, 0);
    print(date.day);

    setState(() {
      _listDateBirthdate.clear();
    });

    for (var i = 1; i <= date.day; i++) {
      _listDateBirthdate.add(i.toString());
    }
    setState(() {
      if (blnLahirSelected == 3) {
        tglLahir = "1";
      }
    });
  }

  _showDialog() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Anda yakin untuk mengajukan kredit ini?",
                style: TextStyle(fontFamily: "NunitoSans")),
            actions: <Widget>[
              FlatButton(
                  onPressed: _validatesubmit == true
                      ? null
                      : () {
                          // _isProcessCheckVoucher == true
                          //     ? _cekplafond()
                          //     : _processData();
                        },
                  child: Text("Lanjutkan",
                      style: TextStyle(
                          fontFamily: "NunitoSansBold", color: Colors.yellow))),
              FlatButton(
                  onPressed: () {
                    // Navigator.pop(context);
                    // _saveToDraft();
                  },
                  child: Text("Simpan di draft",
                      style: TextStyle(
                          fontFamily: "NunitoSans", color: Colors.yellow))),
            ],
          );
        });
  }

  void _showSnackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text("Data tidak ditemukan"),
        behavior: SnackBarBehavior.floating));
  }

  _backToHomePage() {
    Navigator.pop(context, true);
  }

  bool _dpIsBiggerThenOTR() {
    // var _otr = _controllerOTR.text;
    // var _dp = _controllerDP.text;
    // if (_otr == "") {
    //   _otr = "19999999";
    // }
    // if (_dp == "") {
    //   _dp = "9999999";
    // }
    // int _otrCompare = int.parse(_otr.replaceAll(",", ""));
    // int _dpCompare = int.parse(_dp.replaceAll(",", ""));
    // if (_dpCompare == 100) {
    //   print("dp tidakboleh lebih atau sama dengan otr");
    //   return true;
    // } else {
    //   if (_dpCompare > _otrCompare) {
    //     return true;
    //   } else {
    //     return false;
    //   }
    // }
  }

  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;
    });
    if (_radioValue == 1) {
      setState(() {
        _enableAlamatKTP = true;
      });
    } else {
      setState(() {
        _enableAlamatKTP = false;
      });
    }
  }

  void _handleRadioCustTypeValueChange(int value) {
    setState(() {
      _radioCustTypeValue = value;
    });
    if (_radioCustTypeValue == 1) {
      setState(() {
        _enableAlamatKTP = true;
      });
    } else {
      setState(() {
        _enableAlamatKTP = false;
      });
    }
  }

  _cekZeroPhoneNumber(String zero) {
    if (zero == "0") {
      _controllerNoHp.clear();
    } else {
      return;
    }
  }

  var idMerkKendaraan;
  var idTypeKendaraan;
  var idModelKendaraan;

  _setValueModel(Map _modelKendaraan) {
    print("data model kendaraan $_modelKendaraan");
    setState(() {
      idMerkKendaraan = _modelKendaraan['OBBR_CODE'];
      idTypeKendaraan = _modelKendaraan['OBTY_CODE'];
      idModelKendaraan = _modelKendaraan['OBMO_CODE'];
      _controllerModelKendaraan.text = _modelKendaraan['OBBR_DESC'];
      _controllerTipeKendaraan.text = _modelKendaraan['OBTY_DESC'];
      _controllerMerkKendaraan.text = _modelKendaraan['OBMO_DESC'];
    });
  }

  GenerateDateApiProvider _genenerateDateApiProvider;
  List<String> _dateListFix = [];

  _getGenerateDate() async {
    if (_pemilihanCaraSurvey._id == 1) {
      setState(() {
        _listBranch.clear();
        _dateListFix.clear();
        _listHour.clear();
        _pilihanCabang = null;
        _surveyDateSelected = null;
        _surveyJamSelected = null;
        _pilihanMktDedicated = null;
      });

      var _result = await _genenerateDateApiProvider.generateDatesByAutoAssign(
          _controllerKodePos.text,
          _controllerKelurahan.text.trim(),
          _jenisPembiayaan._id.toString());
      if (_result['status']) {
        var _temp = [];
        var _listDate = [];
        var _lisTimes = [];
        var _listDateAndTimeAutoAssign = [];

        setState(() {
          _temp.clear();
        });

        for (var i = 0; i < _result['listDateByAutoAsign'].length; i++) {
          var _formatDate = _result['listDateByAutoAsign'][i]['dates'];
          print("cek date awal ${_result['listDateByAutoAsign'][i]['dates']}");
          var _dates = _formatDate.split(" ");
          _listDate.add(_dates[0]);
          print(_listDate[i]);
        }

        for (var i = 0; i < _result['listDateByAutoAsign'].length; i++) {
          var _formatTime = _result['listDateByAutoAsign'][i]['dates'];
          var _times = _formatTime.split(" ");
          _lisTimes.add(_times[1]);
        }

        for (var i = 0; i < _result['listDateByAutoAsign'].length; i++) {
          var _data = {"tanggal": _listDate[i], "jam": _lisTimes[i]};
          _listDateAndTimeAutoAssign.add(_data);
          print(_listDateAndTimeAutoAssign[i]);
        }

        var _checkDateAutoAssign = await _dbHelper.getDateTimeAutoAssign();
        if (_checkDateAutoAssign.isNotEmpty || _checkDateAutoAssign != null) {
          await _dbHelper.deleteDateAutoAssign();
          await _dbHelper.addDateAutoAssign(_listDateAndTimeAutoAssign);
        } else {
          await _dbHelper.addDateAutoAssign(_listDateAndTimeAutoAssign);
        }

        var _resultDateAutoAssign = await _dbHelper.getDateAutoAssign();
        for (var i = 0; i < _resultDateAutoAssign.length; i++) {
          if (!_dateListFix.contains(_resultDateAutoAssign[i]['tanggal'])) {
            setState(() {
              _dateListFix.add(_resultDateAutoAssign[i]['tanggal']);
            });
          }
        }
      } else {
        _showSnackBar(_result['data']['error_description']);
      }
    } else if (_pemilihanCaraSurvey._id == 2) {
      setState(() {
        _loadData = true;
        _listBranch.clear();
        _dateListFix.clear();
        _listHour.clear();
        _pilihanCabang = null;
        _surveyDateSelected = null;
        _surveyJamSelected = null;
        _pilihanMktDedicated = null;
      });
      var _result =
          await _genenerateDateApiProvider.generateDatesByPemilihanCabang(
              _controllerKodePos.text,
              _controllerKelurahan.text.trim(),
              _jenisPembiayaan._id.toString());
      print("check auto branch ${_result['listGenerateDateByBranch']}");
      if (_result['status']) {
        var _temp = [];

        setState(() {
          _temp.clear();
        });

        var _listDate = [];
        var _lisTimes = [];
        var _listDataDateAndBranch = [];
        for (var i = 0; i < _result['listGenerateDateByBranch'].length; i++) {
          var _formatDate =
              _result['listGenerateDateByBranch'][i]['dates'].trim();
          var _dates = _formatDate.split(" ");
          _listDate.add(_dates[0]);
        }
        for (var i = 0; i < _result['listGenerateDateByBranch'].length; i++) {
          var _formatTime =
              _result['listGenerateDateByBranch'][i]['dates'].trim();
          var _times = _formatTime.split(" ");
          _lisTimes.add(_times[1]);
        }
        for (var i = 0; i < _result['listGenerateDateByBranch'].length; i++) {
          var _data = {
            "branchId": _result['listGenerateDateByBranch'][i]['branchId'],
            "branchName": _result['listGenerateDateByBranch'][i]['BranchName'],
            "date": _listDate[i],
            "time": _lisTimes[i]
          };
          _listDataDateAndBranch.add(_data);
        }
        var _cekDataDateAndBranch = await _dbHelper.getDateAndBranchDate();
        if (_cekDataDateAndBranch.isNotEmpty || _cekDataDateAndBranch != null) {
          await _dbHelper.deleteDataDateAndGenerate();
          await _dbHelper.addDateAndBranchdata(_listDataDateAndBranch);
        } else {
          await _dbHelper.addDateAndBranchdata(_listDataDateAndBranch);
        }
        for (var i = 0; i < _result['listGenerateDateByBranch'].length; i++) {
          var _dataDealer = {
            "branchId": _result['listGenerateDateByBranch'][i]['branchId'],
            "BranchName": _result['listGenerateDateByBranch'][i]['BranchName']
          };
          _temp.add(_dataDealer);
        }

        var uniqueIds = _temp.map((o) => o["branchId"].trim()).toSet().toList();
        var uniqueName =
            _temp.map((o) => o["BranchName"].trim()).toSet().toList();
        Branch _branch;
        for (var i = 0; i < uniqueIds.length; i++) {
          var _myData = {"branchId": uniqueIds[i], "BranchName": uniqueName[i]};
          _branch = Branch(uniqueIds[i], uniqueName[i]);
          setState(() {
            _listBranch.add(_branch);
          });
        }
        setState(() {
          _loadData = false;
        });
      } else {
        setState(() {
          _loadData = false;
        });
        _showSnackBar(_result['data']['error_description']);
      }
    }
    /*else if(_pemilihanCaraSurvey._id == 3){
      print("dijalankan");
      var _result = await _genenerateDateApiProvider.generateDatesByDadicateCMO(
          _controllerKodePos.text, _controllerKelurahan.text.trim(), _jenisPembiayaan._id.toString());

      if(_result['status']){
        print("cek_result ${_result['listDateByDedicateCMO']}");
        setState(() {
          _listDealerCabang.clear();
          _dateListFix.clear();
          _listHour.clear();
          _pilihanCabang = null;
          _surveyDateSelected = null;
          _surveyJamSelected = null;
        });

        var _listDate = [];
        var _lisTimes = [];
        var _listDateTimeByDedicateCMO = [];

        for(var i=0; i < _result['listDateByDedicateCMO'].length; i++){
          var _formatDate = _result['listDateByDedicateCMO'][i]['dates'];
          var _dates = _formatDate.split(" ");
          _listDate.add(_dates[0]);
        }

        for(var i=0; i< _result['listDateByDedicateCMO'].length; i++){
          var _formatTime = _result['listDateByDedicateCMO'][i]['dates'];
          var _times = _formatTime.split(" ");
          _lisTimes.add(_times[1]);
        }

        for(var i=0; i< _result['listDateByDedicateCMO'].length; i++){
          var _data = {
            "tanggal": _listDate[i],
            "jam":_lisTimes[i]
          };
          _listDateTimeByDedicateCMO.add(_data);
        }

        var _checkDateTimeByDedicatedCMO = await _dbHelper.getDateTimeByDedicatedCMO();
        if(_checkDateTimeByDedicatedCMO.isNotEmpty || _checkDateTimeByDedicatedCMO !=  null){
          await _dbHelper.deleteDateTimeByDedicatedCMO();
          await _dbHelper.addDateTimeByDedicatedCMO(_listDateTimeByDedicateCMO);
        }
        else{
          await _dbHelper.addDateTimeByDedicatedCMO(_listDateTimeByDedicateCMO);
        }

        var _resultDateDecicatedCMO = await _dbHelper.getDateByDedicatedCMO();
        for(var i=0; i < _resultDateDecicatedCMO.length; i++){
          if(!_dateListFix.contains(_resultDateDecicatedCMO[i]['tanggal'])){
            setState(() {
              _dateListFix.add(_resultDateDecicatedCMO[i]['tanggal']);
            });
          }
        }
      }
      else{
        print("data salah");
      }
    }*/
    //edit DAK
    else if (_pemilihanCaraSurvey._id == 5) {
      setState(() {
        _loadData = true;
        _listMkt.clear();
        _listDealerCabang.clear();
        _dateListFix.clear();
        _listHour.clear();
        _pilihanCabang = null;
        _surveyDateSelected = null;
        _surveyJamSelected = null;
        _pilihanMktDedicated = null;
      });

      var _result = await _genenerateDateApiProvider.generateDatesByNbm(
          _controllerKodePos.text,
          _controllerKelurahan.text.trim(),
          _jenisPembiayaan._id.toString());

      if (_result['status']) {
        print("cek_result ${_result['listDateByNbm']}");

        var _listDate = [];
        var _lisTimes = [];
        var _listDateTimeByNbm = [];
        var _temp = [];

        for (var i = 0; i < _result['listDateByNbm'].length; i++) {
          var _formatDate = _result['listDateByNbm'][i]['dates'];
          var _dates = _formatDate.split(" ");
          _listDate.add(_dates[0]);
        }

        for (var i = 0; i < _result['listDateByNbm'].length; i++) {
          var _formatTime = _result['listDateByNbm'][i]['dates'];
          var _times = _formatTime.split(" ");
          _lisTimes.add(_times[1]);
        }

        for (var i = 0; i < _result['listDateByNbm'].length; i++) {
          var _data = {
            "nik": _result['listDateByNbm'][i]['NIK'],
            "nama": _result['listDateByNbm'][i]['EmployeeName'],
            "tanggal": _listDate[i],
            "jam": _lisTimes[i]
          };
          _listDateTimeByNbm.add(_data);
        }

        var _checkDateTimeByNbm = await _dbHelper.getDateTimeByNbm();
        if (_checkDateTimeByNbm.isNotEmpty || _checkDateTimeByNbm != null) {
          await _dbHelper.deleteDateTimeByNbm();
          await _dbHelper.addDateTimeByNbm(_listDateTimeByNbm);
        } else {
          await _dbHelper.addDateTimeByNbm(_listDateTimeByNbm);
        }

        for (var i = 0; i < _result['listDateByNbm'].length; i++) {
          var _dataMarketing = {
            "NIK": _result['listDateByNbm'][i]['NIK'],
            "EmployeeName": _result['listDateByNbm'][i]['EmployeeName']
          };
          _temp.add(_dataMarketing);
        }

        PemilihanMktDedicated dedicated;
        var uniqueIds = _temp.map((o) => o["NIK"].trim()).toSet().toList();
        var uniqueName =
            _temp.map((o) => o["EmployeeName"].trim()).toSet().toList();
        for (var i = 0; i < uniqueIds.length; i++) {
          dedicated = PemilihanMktDedicated(uniqueIds[i], uniqueName[i]);
          setState(() {
            _listMkt.add(dedicated);
          });
        }
        setState(() {
          _loadData = false;
        });
      } else {
        setState(() {
          _loadData = false;
        });
        _showSnackBar(_result['data']['error_description']);
      }
    } else if (_pemilihanCaraSurvey._id == 4) {
      setState(() {
        _loadData = true;
        _listBranch.clear();
        _dateListFix.clear();
        _listHour.clear();
        _pilihanCabang = null;
        _surveyDateSelected = null;
        _surveyJamSelected = null;
        _pilihanMktDedicated = null;
      });
      var _result = await _getDataTrackingApiProvider.getBranchByDLC();
//      var _result = await _genenerateDateApiProvider.generateDatesByAsIs(
//          _controllerKodePos.text, _controllerKelurahan.text.trim(), _jenisPembiayaan._id.toString());
//      if(_result['status']){
      var _temp = [];
      setState(() {
        _temp.clear();
      });
      if (_result['status']) {
        Branch _branch;
        for (var i = 0; i < _result['listBranch'].length; i++) {
          _branch = Branch(_result['listBranch'][i]['SZBRID'],
              _result['listBranch'][i]['SZBRANCHNAME']);
          _listBranch.add(_branch);
//          _listDealerCabang.add(_result['listBranch'][i]);
        }
        setState(() {
          _loadData = false;
        });
      } else {
        setState(() {
          _loadData = false;
        });
        _showSnackBar(_result['data']['error_description']);
      }
//
//        var _listDate = [];
//        var _lisTimes = [];
//        var _listDataDateAndBranch = [];
//        for(var i=0; i<_result['listGenerateDatesByAsIs'].length; i++){
//          var _formatDate = _result['listGenerateDatesByAsIs'][i]['dates'].trim();
//          var _dates = _formatDate.split(" ");
//          _listDate.add(_dates[0]);
//        }
//        for(var i=0; i<_result['listGenerateDatesByAsIs'].length; i++){
//          var _formatTime = _result['listGenerateDatesByAsIs'][i]['dates'].trim();
//          var _times = _formatTime.split(" ");
//          _lisTimes.add(_times[1]);
//        }
//        for(var i =0; i<_result['listGenerateDatesByAsIs'].length; i++){
//          var _data = {
//            "branchId":_result['listGenerateDatesByAsIs'][i]['branchId'],
//            "branchName": _result['listGenerateDatesByAsIs'][i]['BranchName'],
//            "date": _listDate[i],
//            "time":_lisTimes[i]
//          };
//          _listDataDateAndBranch.add(_data);
//        }
//        var _cekDataDateByAsIs = await _dbHelper.getDateTimeByAsIs();
//        if(_cekDataDateByAsIs.isNotEmpty || _cekDataDateByAsIs != null){
//          await _dbHelper.deleteDataDateByAsis();
//          await _dbHelper.addDateByAsIs(_listDataDateAndBranch);
//        }
//        else{
//          await _dbHelper.addDateByAsIs(_listDataDateAndBranch);
//        }
//        for(var i=0; i<_result['listGenerateDatesByAsIs'].length; i++){
//          var _dataDealer = {
//            "branchId":_result['listGenerateDatesByAsIs'][i]['branchId'],
//            "BranchName":_result['listGenerateDatesByAsIs'][i]['BranchName']
//          };
//          _temp.add(_dataDealer);
//        }
//
//
//        var uniqueIds = _temp.map((o) => o["branchId"].trim()).toSet().toList();
//        var uniqueName = _temp.map((o) => o["BranchName"].trim()).toSet().toList();
//        for(var i =0; i<uniqueIds.length; i++){
//          var _myData = {"branchId":uniqueIds[i],"BranchName":uniqueName[i]};
//          print(_myData);
//          setState(() {
//            _listDealerCabang.add(_myData);
//          });
//        }
//      }
    }
  }

  _getDateList(String branchId) async {
    if (_pemilihanCaraSurvey._id == 2) {
      var _resultDate = await _dbHelper.getDateByBranchId(branchId);
      var _lisDateTemp = [];
      if (_lisDateTemp.isNotEmpty || _dateListFix.isNotEmpty) {
        setState(() {
          _lisDateTemp.clear();
          _dateListFix.clear();
          _listHour.clear();
          _surveyDateSelected = null;
          _surveyJamSelected = null;
        });
        for (var i = 0; i < _resultDate.length; i++) {
          _lisDateTemp.add(_resultDate[i]['date']);
        }
        for (var i = 0; i < _lisDateTemp.length; i++) {
          if (!_dateListFix.contains(_lisDateTemp[i])) {
            setState(() {
              _dateListFix.add(_lisDateTemp[i]);
            });
          }
        }
      } else {
        for (var i = 0; i < _resultDate.length; i++) {
          _lisDateTemp.add(_resultDate[i]['date']);
        }
        for (var i = 0; i < _lisDateTemp.length; i++) {
          if (!_dateListFix.contains(_lisDateTemp[i])) {
            setState(() {
              _dateListFix.add(_lisDateTemp[i]);
            });
          }
        }
      }
    } else {
      setState(() {
        _dateListFix.clear();
        _listHour.clear();
        _surveyDateSelected = null;
        _surveyJamSelected = null;
      });
      var _now = DateTime.now();
      DateFormat _formatter = DateFormat("EEEE,dd-MM-yyyy");
      for (var i = 0; i < 6; i++) {
        var _date = DateTime(_now.year, _now.month, _now.day + i);
        String _dateSurvey = _formatter.format(_date);
        _dateListFix.add(_dateSurvey);
      }
//      var _resultDate = await _dbHelper.getDateByAsIs(branchId);
//      var _lisDateTemp = [];
//      if(_lisDateTemp.isNotEmpty || _dateListFix.isNotEmpty){
//        setState(() {
//          _lisDateTemp.clear();
//          _dateListFix.clear();
//          _listHour.clear();
//          _surveyDateSelected = null;
//          _surveyJamSelected = null;
//        });
//        for(var i=0; i<_resultDate.length; i++){
//          _lisDateTemp.add(_resultDate[i]['date']);
//        }
//        for(var i=0; i<_lisDateTemp.length;i++){
//          if(!_dateListFix.contains(_lisDateTemp[i])){
//            setState(() {
//              _dateListFix.add(_lisDateTemp[i]);
//            });
//          }
//        }
//      }
//      else{
//        for(var i=0; i<_resultDate.length; i++){
//          _lisDateTemp.add(_resultDate[i]['date']);
//        }
//        for(var i=0; i<_lisDateTemp.length;i++){
//          if(!_dateListFix.contains(_lisDateTemp[i])){
//            setState(() {
//              _dateListFix.add(_lisDateTemp[i]);
//            });
//          }
//        }
//      }
    }
  }

  List<String> _listHour = [];
  List<String> _listMinute = ["00", "30"];
  _getTimeByDateAutoBranchOrByAsIs(String date) async {
    setState(() {
      _listHour.clear();
      _surveyJamSelected = null;
    });

    if (_pemilihanCaraSurvey._id == 2) {
      var _resultTime =
          await _dbHelper.getTimeByBranchAndDate(_pilihanCabang._id, date);
      var _tempListTime = [];
      for (var i = 0; i < _resultTime.length; i++) {
        String _hour = _resultTime[i]['time'];
        var _splitHour = _hour.split(":");
        _tempListTime.add(_splitHour[0]);
      }
      for (var i = 0; i < _tempListTime.length; i++) {
        print(_tempListTime[i]);
        if (!_listHour.contains(_tempListTime[i]))
          _listHour.add(_tempListTime[i]);
      }
    } else {
      for (var i = 6; i < 22; i++) {
        _listHour.add("$i");
      }
//      var _resultTime = await _dbHelper.getTimeByDateAsIs(_pilihanCabang, date);
//      var _tempListTime = [];
//      for(var i= 0; i<_resultTime.length;i++){
//        String _hour = _resultTime[i]['time'];
//        var _splitHour = _hour.split(":");
//        _tempListTime.add(_splitHour[0]);
//      }
//      for(var i= 0; i<_tempListTime.length;i++){
//        print(_tempListTime[i]);
//        if(!_listHour.contains(_tempListTime[i]))
//          _listHour.add(_tempListTime[i]);
//      }
    }
  }

  _getTimeByDateAutoAssign(String date) async {
    setState(() {
      _listHour.clear();
      _surveyJamSelected = null;
    });
    var _resultTime = await _dbHelper.getTimeByDateAutoAssign(date);
    var _tempListTime = [];
    for (var i = 0; i < _resultTime.length; i++) {
      String _hour = _resultTime[i]['jam'];
      var _splitHour = _hour.split(":");
      _tempListTime.add(_splitHour[0]);
    }
    for (var i = 0; i < _tempListTime.length; i++) {
      print(_tempListTime[i]);
      if (!_listHour.contains(_tempListTime[i]))
        setState(() {
          _listHour.add(_tempListTime[i]);
        });
    }
  }

  _getTimeByNbm(String nik, String date) async {
    setState(() {
      _listHour.clear();
      _surveyJamSelected = null;
    });
    var _resultTime = await _dbHelper.getTimesByNbmByNikAndDate(nik, date);
    var _tempListTime = [];
    for (var i = 0; i < _resultTime.length; i++) {
      String _hour = _resultTime[i]['jam'];
      var _splitHour = _hour.split(":");
      _tempListTime.add(_splitHour[0]);
    }
    for (var i = 0; i < _tempListTime.length; i++) {
      print(_tempListTime[i]);
      if (!_listHour.contains(_tempListTime[i]))
        setState(() {
          _listHour.add(_tempListTime[i]);
        });
    }
    setState(() {
      _surveyMenitSelected = _listMinute[0];
    });
  }

  _getDateByNbmByNik(String nik) async {
    var _result = await _dbHelper.getDateByNbmByNik(nik);
    var _listTemp = [];
    for (var i = 0; i < _result.length; i++) {
      _listTemp.add(_result[i]['tanggal']);
    }
    for (var i = 0; i < _listTemp.length; i++) {
      if (!_dateListFix.contains(_listTemp[i])) {
        setState(() {
          _dateListFix.add(_listTemp[i]);
        });
      }
    }
  }

  setAddressByMap(Map value) {
    setState(() {
      _controllerLocation.text = value['address'];
      _longitude = value['longitude'].toString();
      _latitude = value['latitude'].toString();
    });
  }
}

class Gender {
  final String id;
  final String genderName;
  Gender(this.id, this.genderName);
}

class FormDataNasabah {
  final String formId;
  final String formDesc;
  final bool value;
  FormDataNasabah(this.formId, this.formDesc, this.value);
}

class JenisKendaraan {
  final String id;
  final String jenisKendaraan;
  JenisKendaraan(this.id, this.jenisKendaraan);
}

class JenisPembiayaan {
  final int _id;
  final String _jenisPembiayaanName;

  JenisPembiayaan(this._id, this._jenisPembiayaanName);
}

class ProdukPembiayaan {
  final int _id;
  final String _produkPembiayaanName;

  ProdukPembiayaan(this._id, this._produkPembiayaanName);
}

class JenisAsuransi {
  final int _id;
  final String _jenisAsuransiName;

  JenisAsuransi(this._id, this._jenisAsuransiName);
}

class PemilihanCaraSurvey {
  final int _id;
  final String _pemilihanCaraSurvey;

  PemilihanCaraSurvey(this._id, this._pemilihanCaraSurvey);
}

class Branch {
  final String _id;
  final String _branchName;

  Branch(this._id, this._branchName);
}

class PemilihanMktDedicated {
  final String _nik;
  final String _employeeName;

  PemilihanMktDedicated(this._nik, this._employeeName);
}
